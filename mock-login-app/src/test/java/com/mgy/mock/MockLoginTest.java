package com.mgy.mock;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

/**
 * xxx
 *
 * @author maguoyong
 * @date 2020/7/6
 */
public class MockLoginTest {


    /**
     * 安卓6.0.1
     */
    @Test
    public void test1() throws InterruptedException, MalformedURLException {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(CapabilityType.BROWSER_NAME, "");
        //指定测试平台
        cap.setCapability("platformName", "Android");
        //指定测试机的ID,通过adb命令`adb devices`获取
        cap.setCapability("deviceName", "4af99c7"); //XPL0220118018558    oppo 4af99c7
        //真机安卓版本
        cap.setCapability("platformVersion", "6.0.1");  //10         6.0.1
        cap.setCapability(CapabilityType.VERSION, "6.0.1");
        cap.setCapability("appPackage", "com.UCMobile");
        cap.setCapability("appActivity", "com.uc.browser.InnerUCMobile");
//        cap.setCapability("appWaitActivity", "");
        cap.setCapability("sessionOverride", true);
        AppiumDriver<WebElement> driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);

        System.out.println("-------------------测试--------------------");
        Thread.sleep(3000);
        System.out.println("-------------------测试2--------------------");
//        driver.findElementsById("").get(0).click();
        driver.findElementsByLinkText("菜单").get(0).click();
//
//        Thread.sleep(3000);
//        int width = driver.manage().window().getSize().width;
//        int height = driver.manage().window().getSize().height;
//        int x0 = (int) (width * 0.8);  // 起始x坐标
//        int x1 = (int) (height * 0.2);  // 终止x坐标
//        int y = (int) (height * 0.5);  // y坐标
//        for (int i = 0; i < 5; i++) {
//            driver.swipe(x0, y, x1, y, 500);
//            Thread.sleep(1000);
//        }
//
//        driver.findElementById("com.youdao.calculator:id/guide_button").click();
//        for (int i = 0; i < 6; i++) {
//            driver.findElementByXPath("//android.webkit.WebView[@text='Mathbot Editor']").click();
//            Thread.sleep(1000);
//        }
//
//        String btn_xpath = "//*[@resource-id='com.youdao.calculator:id/view_pager_keyboard']/android.widget.GridView/android.widget.FrameLayout[%d]/android.widget.FrameLayout";
//        driver.findElementByXPath(String.format(btn_xpath, 7)).click();
//        driver.findElementByXPath(String.format(btn_xpath, 10)).click();
//        driver.findElementByXPath(String.format(btn_xpath, 8)).click();
//        Thread.sleep(3000);


    }

    /**
     * 安卓10
     */
    @Test
    public void test2() throws MalformedURLException {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(CapabilityType.BROWSER_NAME, "");
        //指定测试平台
        cap.setCapability("platformName", "Android");
        //指定测试机的ID,通过adb命令`adb devices`获取
        cap.setCapability("deviceName", "XPL0220118018558"); //XPL0220118018558    oppo 4af99c7
        //真机安卓版本
        cap.setCapability("platformVersion", "10");  //10         6.0.1
        cap.setCapability(CapabilityType.VERSION, "10");
        cap.setCapability("appPackage", "com.UCMobile");
        cap.setCapability("appActivity", "com.uc.browser.InnerUCMobile");
//        cap.setCapability("appWaitActivity", "");
        cap.setCapability("sessionOverride", true);
        AppiumDriver<WebElement> driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);

        driver.findElementsByLinkText("菜单").get(0).click();
    }


    /**
     * 安卓10 淘宝直播
     */
    @Test
    public void test3() throws MalformedURLException, InterruptedException {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(CapabilityType.BROWSER_NAME, "");
        //指定测试平台
        cap.setCapability("platformName", "Android");
        //指定测试机的ID,通过adb命令`adb devices`获取
        cap.setCapability("deviceName", "XPL0220118018558"); //XPL0220118018558    oppo 4af99c7
        //真机安卓版本
        cap.setCapability("platformVersion", "10");  //10         6.0.1
        cap.setCapability(CapabilityType.VERSION, "10");
        cap.setCapability("appPackage", "com.taobao.live");
//        cap.setCapability("appActivity", "com.taobao.live.home.activity.HomePage3Activity");
        cap.setCapability("appActivity", "com.taobao.live.SplashActivity");
        //com.ali.user.mobile.login.ui.UserLoginActivity
//        cap.setCapability("appActivity", "com.ali.user.mobile.login.ui.UserLoginActivity");
//        cap.setCapability("appWaitActivity", "");
        cap.setCapability("sessionOverride", true);
        //不清除应用启动数据，默认清理false
        cap.setCapability("noReset", true);
        AndroidDriver<AndroidElement> androidDriver = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        Thread.sleep(3000);
        System.out.println("点击我的");


        //NoSuchElementException
//        WebElement my = androidDriver.findElement(By.id("com.taobao.live:id/hp3_tab_img"));
        System.out.println("是否锁屏：" + androidDriver.isDeviceLocked());


        AndroidElement container = androidDriver.findElementById("com.taobao.live:id/layermanager_penetrate_webview_container_id");
//        String xpath = "//android.widget.TextView[@text='新用户登录抽红包']/..";
//        container.findElementByXPath("")


        MobileElement closeElement = container.findElementByXPath("//android.widget.ImageView");
        closeElement.click();

        try {
            //方法一
//            AndroidElement my = (AndroidElement) androidDriver.findElementById("com.taobao.live:id/hp3_tab_img");
//            my.click();
            //方法二
//            String xpath = "//android.widget.ImageView[@resource-id='com.taobao.live:id/hp3_tab_img']/..";
//            AndroidElement my = (AndroidElement) androidDriver.findElementByXPath(xpath);
//            my.click();
//方法三
//            AndroidElement my= (AndroidElement)  androidDriver.findElementByAndroidUIAutomator("new UiSelector().resourceId(\"com.taobao.live:id/hp3_tab_img\")");
//            my.click();
            //方法四
            String xpath = "//android.widget.TextView[@text='新用户登录抽红包']/..";
            AndroidElement my = (AndroidElement) androidDriver.findElementByXPath(xpath);
            my.click();

            Thread.sleep(3000);
        } catch (org.openqa.selenium.NoSuchElementException e) {
            System.out.println("不存在元素：com.taobao.live:id/hp3_tab_img");
        }

        System.out.println("登录账号");
        try {
            androidDriver.findElementById("com.taobao.live:id/ali_user_guide_account_login_btn").click();
        } catch (org.openqa.selenium.NoSuchElementException e) {
            System.out.println("不存在元素：com.taobao.live:id/ali_user_guide_account_login_btn");
        }
        Thread.sleep(3000);

        String userName = "xxxxxx";
        String password = "xxxxxx";


        try {
            AndroidElement usernameElement = androidDriver.findElementById("com.taobao.live:id/aliuser_login_account_et");
            // 模拟用户点击用户名输入框
            usernameElement.click();
            //输入账号
            pressAccount(androidDriver);


            AndroidElement passwordElement = androidDriver.findElementById("com.taobao.live:id/aliuser_login_password_et");
            passwordElement.click();
            // 输入完成用户名后，随机睡眠0-3秒
            pressPassword(androidDriver);

            //点击登录
            androidDriver.findElementById("com.taobao.live:id/aliuser_login_login_btn");
            Thread.sleep(2000);
        } catch (Exception e) {
            androidDriver.quit();
        }
    }



    /**
     * 安卓5.1.1 淘宝直播
     */
    @Test
    public void test4() throws MalformedURLException, InterruptedException {
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(CapabilityType.BROWSER_NAME, "");
        //指定测试平台
        cap.setCapability("platformName", "Android");
        //指定测试机的ID,通过adb命令`adb devices`获取
        cap.setCapability("deviceName", "127.0.0.1:62001 device"); //XPL0220118018558    oppo 4af99c7
        cap.setCapability("udid","127.0.0.1:62001");
        //真机安卓版本
        cap.setCapability("platformVersion", "5.1.1");  //10         6.0.1
        cap.setCapability(CapabilityType.VERSION, "5.1.1");
        cap.setCapability("appPackage", "com.taobao.live");
//        cap.setCapability("appActivity", "com.taobao.live.home.activity.HomePage3Activity");
        cap.setCapability("appActivity", "com.taobao.live.SplashActivity");
        //com.ali.user.mobile.login.ui.UserLoginActivity
//        cap.setCapability("appActivity", "com.ali.user.mobile.login.ui.UserLoginActivity");
//        cap.setCapability("appWaitActivity", "");
        cap.setCapability("sessionOverride", true);
        //不清除应用启动数据，默认清理false
        cap.setCapability("noReset", true);
        AndroidDriver<AndroidElement> androidDriver = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
        Thread.sleep(3000);
        System.out.println("点击我的");


        //NoSuchElementException
//        WebElement my = androidDriver.findElement(By.id("com.taobao.live:id/hp3_tab_img"));
        System.out.println("是否锁屏：" + androidDriver.isDeviceLocked());


        AndroidElement container = androidDriver.findElementById("com.taobao.live:id/layermanager_penetrate_webview_container_id");
//        String xpath = "//android.widget.TextView[@text='新用户登录抽红包']/..";
//        container.findElementByXPath("")


        MobileElement closeElement = container.findElementByXPath("//android.widget.ImageView");
        closeElement.click();

        try {
            //方法一
//            AndroidElement my = (AndroidElement) androidDriver.findElementById("com.taobao.live:id/hp3_tab_img");
//            my.click();
            //方法二
//            String xpath = "//android.widget.ImageView[@resource-id='com.taobao.live:id/hp3_tab_img']/..";
//            AndroidElement my = (AndroidElement) androidDriver.findElementByXPath(xpath);
//            my.click();
//方法三
//            AndroidElement my= (AndroidElement)  androidDriver.findElementByAndroidUIAutomator("new UiSelector().resourceId(\"com.taobao.live:id/hp3_tab_img\")");
//            my.click();
            //方法四
            String xpath = "//android.widget.TextView[@text='新用户登录抽红包']/..";
            AndroidElement my = (AndroidElement) androidDriver.findElementByXPath(xpath);
            my.click();

            Thread.sleep(3000);
        } catch (org.openqa.selenium.NoSuchElementException e) {
            System.out.println("不存在元素：com.taobao.live:id/hp3_tab_img");
        }

        System.out.println("登录账号");
        try {
            androidDriver.findElementById("com.taobao.live:id/ali_user_guide_account_login_btn").click();
        } catch (org.openqa.selenium.NoSuchElementException e) {
            System.out.println("不存在元素：com.taobao.live:id/ali_user_guide_account_login_btn");
        }
        Thread.sleep(3000);

        String userName = "xxxxxx";
        String password = "xxxxxx";


        try {
            AndroidElement usernameElement = androidDriver.findElementById("com.taobao.live:id/aliuser_login_account_et");
            // 模拟用户点击用户名输入框
            usernameElement.click();
            //输入账号
            pressAccount(androidDriver);


            AndroidElement passwordElement = androidDriver.findElementById("com.taobao.live:id/aliuser_login_password_et");
            passwordElement.click();
            // 输入完成用户名后，随机睡眠0-3秒
            pressPassword(androidDriver);

            //点击登录
            androidDriver.findElementById("com.taobao.live:id/aliuser_login_login_btn");
            Thread.sleep(2000);
        } catch (Exception e) {
            androidDriver.quit();
        }
    }




    private void pressAccount(AndroidDriver<AndroidElement> androidDriver) throws InterruptedException {
        Random rand = new Random();
        KeyEvent no1 = new KeyEvent();
        no1.withKey(AndroidKey.DIGIT_1);
        androidDriver.pressKey(no1);
        Thread.sleep(rand.nextInt(1000));

        KeyEvent no2 = new KeyEvent();
        no2.withKey(AndroidKey.DIGIT_2);
        androidDriver.pressKey(no2);
        Thread.sleep(rand.nextInt(1000));

        KeyEvent no3 = new KeyEvent();
        no3.withKey(AndroidKey.DIGIT_3);
        androidDriver.pressKey(no3);
        Thread.sleep(rand.nextInt(1000));

        KeyEvent no4 = new KeyEvent();
        no4.withKey(AndroidKey.DIGIT_4);
        androidDriver.pressKey(no4);
        Thread.sleep(rand.nextInt(1000));


        //@符号
        KeyEvent at = new KeyEvent();
        at.withKey(AndroidKey.AT);
        androidDriver.pressKey(at);
        Thread.sleep(rand.nextInt(1000));

        //字母
        KeyEvent a = new KeyEvent();
        a.withKey(AndroidKey.A);
        androidDriver.pressKey(a);
        Thread.sleep(rand.nextInt(1000));


        //小数点 .
        KeyEvent period = new KeyEvent();
        period.withKey(AndroidKey.PERIOD);
        androidDriver.pressKey(period);
        Thread.sleep(rand.nextInt(1000));

    }

    private void pressPassword(AndroidDriver<AndroidElement> androidDriver) throws InterruptedException {

        Random rand = new Random();

        KeyEvent a = new KeyEvent();
        a.withKey(AndroidKey.A);
        androidDriver.pressKey(a);
        Thread.sleep(rand.nextInt(1000));


        KeyEvent b = new KeyEvent();
       b.withKey(AndroidKey.B);
        androidDriver.pressKey(b);
        Thread.sleep(rand.nextInt(1000));
    }

    private boolean isExistElement(AndroidDriver<AndroidElement> androidDriver, String resourceId) {
        try {
            androidDriver.findElementById(resourceId);
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            System.out.println("不存在元素" + resourceId);
        }
        return false;
    }
}
