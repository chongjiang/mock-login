package com.mgy.mock;

import com.mgy.common.TLSSocketConnectionFactory;
import org.jsoup.Jsoup;
import org.jsoup.helper.HttpConnection;
import org.junit.Test;

import java.io.IOException;
import java.net.Authenticator;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class JsoupTest {


    public String getUA() {
        String[] ua = {"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36",
                "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36 OPR/37.0.2178.32",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586",
                "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko",
                "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)",
                "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)",
                "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0)",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 BIDUBrowser/8.3 Safari/537.36",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.80 Safari/537.36 Core/1.47.277.400 QQBrowser/9.4.7658.400",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 UBrowser/5.6.12150.8 Safari/537.36",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.122 Safari/537.36 SE 2.X MetaSr 1.0",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36 TheWorld 7",
                "Mozilla/5.0 (Windows NT 6.1; W…) Gecko/20100101 Firefox/60.0"};

        int index = new Random().nextInt(ua.length - 1);
        return ua[index];
    }

    public Map<String, String> getCommonHeaders() {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("user-agent", getUA());
        headerMap.put("sec-ch-ua", "\"Chromium\";v=\"128\", \"Not;A=Brand\";v=\"24\", \"Google Chrome\";v=\"128\"");
        headerMap.put("sec-ch-ua-platform", "\"macOS\"");
        headerMap.put("sec-fetch-dest", "document");
        headerMap.put("sec-fetch-site", "same-site");
        headerMap.put("sec-fetch-mode", "navigate");
        headerMap.put("sec-fetch-user", "?1");
        headerMap.put("upgrade-insecure-requests", "1");
        headerMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7");
        headerMap.put("accept-language", "zh-CN,zh;q=0.9");
        headerMap.put("cache-control", "max-age=0");
        headerMap.put("priority", "u=0, i");

//        // 设置IP切换头
//        String proxyHeadKey = "Proxy-Tunnel";
//        // 设置Proxy-Tunnel
//        Random random = new Random();
//        int tunnel = random.nextInt(10000);
//        String proxyHeadVal = String.valueOf(tunnel);
//        headerMap.put(proxyHeadKey, proxyHeadVal);

        return headerMap;
    }


    public Map<String, String> getCookies() {
        String cookie = "cna=0BpUH3UkW1ACAbeGbtLUYnRv; xlly_s=1; cookie2=10623ee798e08478a6f130f80b8e683e; t=bc579b58c82d66d448ab102ad20ae913; _tb_token_=e6a7147ad38ee; mtop_partitioned_detect=1; _m_h5_tk=f64203f363c3e7f66cf5ebc277cfa6b0_1724739870665; _m_h5_tk_enc=e6c051a048cb099252c11eb99f5bd6a5; tfstk=ffdIgO4km3dZfJY1q93NCOGKqr5WFHGqVz_JoUFUy6Cd2d_PuuEJyDxWNGLCYJyF8_95XH6ya2VyCzTDf36o-gSRV38JLVlqgeYhZ_KS0jlqld8t0Bs84_elqz-r0mlq_vUONDi2UFY3JN_lXwFL97316aQlJ_BLpN31yarLeWKJWV_lXwedv7CT6Z7Y27LW_P_-dMYIWLiQg5Fd2eI_GFA1JsyHJGNzaC91diFPf7FJ1wTgAZw39js9LLdF6BnTm1LDQHQ1Gba55KTO9wtoiWfpkFpRdQMzogv6WKW6Iyuh5BTXNZ6TX2skTHO5KIn4sMp6sQ1eQDNVqTpwttAEXW1vne5eeh38wg9OygP4gi6JR8a1n7_10Vg_E8k12HmtHXToM9QGRxus5kplpNb1SVg_3UXdSwgS5VZpE; isg=BOrqTs4ceGjduPSEiMN-pGxxO1aMW2615cC003SjiT3Ip4phXOifxAnVM9O7V-ZN";

        Map<String, String> cookieMap = new HashMap<>();
        String[] arr = cookie.split("; ");
        for (String s : arr) {
            String[] kv = s.split("=");
            cookieMap.put(kv[0], kv[1]);
        }
        return cookieMap;
     }


    public Map<String, String> getCookies(String proxyIp, int proxyPort) throws IOException {
        String url = "http://xstore.insights.1688.com/index.html?at_iframe=1&versionId=CAEQFBiBgMDcoZfp1BciIGViNWE3YWZjN2EwMTRiYmZiODY5MWE4ZThkYTUwM2Zl";

//        Authenticator.setDefault(new ProxyAuthenticator("O24082710442845928989", "7d262279006449b388690e763a882459"));
//        System.setProperty("https.proxyHost", proxyIp);
//        System.setProperty("https.proxyPort", proxyPort + "");
//        System.setProperty("http.proxyHost", proxyIp);
//        System.setProperty("http.proxyPort", proxyPort + "");

        Map<String, String> headerMap = getCommonHeaders();

        // 设置代理
//        Proxy proxy = null;
//        if (StringUtils.isNotBlank(proxyIp)) {
//            proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyIp, proxyPort));
//        }

        HttpConnection conn = (HttpConnection) Jsoup.connect(url).timeout(10000).ignoreContentType(true).headers(headerMap);
        conn.execute();
        Map<String, String> map = conn.response().cookies();

        map.forEach((k, v) -> {
            System.out.println(k + "=" + v);
        });

        System.out.println(conn.response().body());
        return map;
    }


    @Test
    public void test1() throws IOException {
        String proxyIp = "flow.hailiangip.com";
        int proxyPort = 14223;
        String url = "https://www.baidu.com";


        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("O24082710442845928989", "pwd=7AV897QQ&pid=22&cid=254&sip=0&uid=&dip=1".toCharArray());
            }
        };
        Authenticator.setDefault(authenticator);
        System.setProperty("https.proxyHost", proxyIp);
        System.setProperty("https.proxyPort", proxyPort + "");
        System.setProperty("http.proxyHost", proxyIp);
        System.setProperty("http.proxyPort", proxyPort + "");
        System.setProperty("https.proxyUser", "O24082710442845928989");
        System.setProperty("https.proxyPassword", "pwd=7AV897QQ&pid=22&cid=254&sip=0&uid=&dip=1");
        System.setProperty("http.proxyUser", "O24082710442845928989");
        System.setProperty("http.proxyPassword", "pwd=7AV897QQ&pid=22&cid=254&sip=0&uid=&dip=1");
        Map<String, String> headerMap = getCommonHeaders();

        // 设置代理
        Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyIp, proxyPort));

        HttpConnection conn = (HttpConnection) Jsoup.connect(url).timeout(10000).proxy(proxy).sslSocketFactory(new TLSSocketConnectionFactory()).ignoreContentType(true).headers(headerMap);
        String body = conn.execute().body();
        System.out.println(body);
    }


    @Test
    public void cookieTest() throws IOException {
        String proxyIp = "flow.hailiangip.com";
        int proxyPort = 14223;

        getCookies(null, proxyPort);
    }


    @Test
    public void itemTest() throws IOException {
        String url = "http://s.1688.com/youyuan/index.htm?imageAddress=https://img.alicdn.com/imgextra/i1/2455164953/O1CN01oqPWSF1mSWghssHDg_!!0-item_pic.jpg";

//
        Map<String, String> headerMap = getCommonHeaders();
        headerMap.put("referer", "http://www.1688.com/");

        Map<String, String> cookieMap = getCookies();
        String liveDetail = Jsoup.connect(url)
                .ignoreContentType(true)
                .headers(headerMap)
                .cookies(cookieMap)
                .execute().body();

        System.out.println(liveDetail);

    }

}
