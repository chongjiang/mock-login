package com.mgy.mock;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Test;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class StartLocalChromeGetCookieTest {


    /**
     * 先启动浏览器，通过debug模式连接浏览器
     * mac电脑打开谷歌浏览器 open /Applications/Google\\ Chrome.app --args --remote-debugging-port=9222
     */
    public void startChrome(String proxyServer, Consumer<ChromeDriver> consumer) throws IOException, InterruptedException {
//        Process process = Runtime.getRuntime().exec("open '/Applications/Google Chrome.app' --args --remote-debugging-port=9222");
//        boolean b = process.waitFor(30, TimeUnit.SECONDS);
//        System.out.println("process is running: " + b);
//        System.out.println("process is running: " + process.isAlive());


        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/workspace/learning/software/chromedriver-mac-arm64/chromedriver");

        ChromeOptions chromeOptions = new ChromeOptions();
        //配置以下内容可以使出现的滑块通过验证
//        chromeOptions.addArguments("lang=zh-CN,zh,zh-TW,en-US,en");
//        chromeOptions.addArguments("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.35");
        chromeOptions.addArguments("--user-data-dir=/Users/admin/Library/Application Support/Google/Chrome/");
        chromeOptions.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});


        if (StringUtils.isNotBlank(proxyServer)) {
            Proxy proxy = new Proxy().setHttpProxy(proxyServer).setSslProxy(proxyServer).setProxyType(Proxy.ProxyType.MANUAL);
            chromeOptions.setProxy(proxy);
        }
//        chromeOptions.setExperimentalOption("debuggerAddress", "127.0.0.1:9222");
//        chromeOptions.addArguments("--proxy-server=https://" + proxyServer);

        ChromeDriver driver = null;
        try {
            driver = new ChromeDriver(chromeOptions);
            //设置隐性等待
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

//            chromeOptions.
            //删除所有cookie
//            driver.manage().deleteAllCookies();


            consumer.accept(driver);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (driver != null) {
                //关闭浏览器
                driver.quit();
            }
        }
    }


    @Test
    public void getCookieTest() throws IOException, InterruptedException {

        String[] urlArr = {"https://www.taobao.com", "https://www.1688.com", "https://www.aliyun.com", "https://etao.com/",
                "https://www.tmall.com/", "https://www.alimama.com/index.htm"};

        startChrome(null, (ChromeDriver driver) -> {
            for (String url : urlArr) {
                try {
                    TimeUnit.SECONDS.sleep(new Random().nextInt(10) + 3);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                driver.get(url);
                Set<Cookie> cookies = driver.manage().getCookies();
                if (cookies != null && cookies.size() > 0) {
                    for (Cookie cookie : cookies) {
                        if (StringUtils.equals(cookie.getName(), "cna")) {
                            System.out.println(url + "\t\t" + cookie.getName() + "=" + cookie.getValue() + ";\t\t" + DateFormatUtils.format(cookie.getExpiry(), "yyyy-MM-dd HH:mm:ss"));
                        }
                    }
                }
                driver.manage().deleteAllCookies();
                driver.getLocalStorage().clear();
                driver.getSessionStorage().clear();
            }
        });

    }
}
