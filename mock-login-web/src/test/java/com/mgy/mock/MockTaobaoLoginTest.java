package com.mgy.mock;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.jsoup.Jsoup;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * xxx
 *如何防止Selenium被检测：https://blog.browserscan.net/zh/docs/how-to-avoid-selenium-detection
 *chromedriver下载： https://googlechromelabs.github.io/chrome-for-testing/#stable
 * @author maguoyong
 * @date 2020/7/6
 */
public class MockTaobaoLoginTest {

    @Test
    public void testMobileWeb() throws InterruptedException {
        //此处填写驱动程序，前面是驱动名随浏览器改变，后面是你对应驱动程序的文件位置，我是绝对路径写法
        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/workspace/learning/software/chromedriver-mac-arm64/chromedriver");


        Map<String, String> mobileEmulation = new HashMap<>();
        mobileEmulation.put("deviceName", "iPhone X");
        Map<String, Object> chromeOptionMap = new HashMap<>();
        chromeOptionMap.put("mobileEmulation", mobileEmulation);
        chromeOptionMap.put("user-agent", "Dalvik/2.1.0 (Linux; U; Android 10; TAS-AN00 Build/HUAWEITAS-AN00)");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptionMap);

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
        chromeOptions.addArguments("User-Agent=Dalvik/2.1.0 (Linux; U; Android 10; TAS-AN00 Build/HUAWEITAS-AN00)");

        chromeOptions.merge(capabilities);

        WebDriver driver = new ChromeDriver(chromeOptions);
        //设置隐性等待
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //网址窗口最大化
//        driver.manage().window().maximize();
        //此处填写需要打开的网址，这是淘宝的登录网址
        driver.get("https://login.taobao.com/member/login.jhtml?spm=a21tn.8216370.754894437.1.349d5722vmf9o6&f=top&redirectURL=https%3A%2F%2Ftaobaolive.taobao.com%2Froom%2Findex.htm");

        String js1 = "Object.defineProperties(navigator,{ webdriver:{ get: () => false } }) ";
        String js2 = "window.navigator.chrome = { runtime: {},  }; ";
        String js3 = "Object.defineProperty(navigator, 'languages', { get: () => ['en-US', 'en'] }); ";
        String js4 = "Object.defineProperty(navigator, 'plugins', { get: () => [1, 2, 3, 4, 5,6], }); ";


        ((ChromeDriver) driver).executeScript(js1);
        ((ChromeDriver) driver).executeScript(js2);
        ((ChromeDriver) driver).executeScript(js3);
        ((ChromeDriver) driver).executeScript(js4);


        System.out.println("打开淘宝登录页面……");
        //获得当前页面的标题
        String tile = driver.getTitle();
        System.out.println("当前页面标题：" + tile);
        //获取当前页面的url
        String url = driver.getCurrentUrl();
        System.out.println("当前页面url" + url);


        // 你的用户名
        String username = "xxxx";
        // 你的密码
        String password = "xxxxx";


        //通过选择器获取元素
        WebElement usernameElement = driver.findElement(By.id("fm-login-id"));
        // 模拟用户点击用户名输入框
        usernameElement.click();
        // 设置sleep,方便观察
        Random rand = new Random();
        try {
            for (int i = 0; i < username.length(); i++) {
                // 随机睡眠0-1秒
                Thread.sleep(rand.nextInt(1000));
                // 逐个输入单个字符
                usernameElement.sendKeys("" + username.charAt(i));
            }
            WebElement passwordElement = driver.findElement(By.id("fm-login-password"));
            passwordElement.click();
            // 输入完成用户名后，随机睡眠0-3秒
            int seconds = rand.nextInt(3000);
            Thread.sleep(seconds);
            for (int i = 0; i < password.length(); i++) {
                Thread.sleep(rand.nextInt(1000));
                passwordElement.sendKeys("" + password.charAt(i));
            }

            //滑块验证
            WebElement wrapper = driver.findElement(By.id("nocaptcha-password"));
            if (wrapper.isDisplayed()) {
                Actions action = new Actions(driver);
                WebElement moveButton = driver.findElement(By.id("nc_1_n1z"));
                // 移到滑块元素并悬停,不能超出框的长度，否则异常
                action.clickAndHold(moveButton);
                action.moveByOffset(258, 0).perform();
                action.release();
            }

            //点击登录
            driver.findElement(By.className("fm-submit")).click();
            System.out.println("登录成功！");
            Thread.sleep(5000);


            //获取cookies
            Set<Cookie> cookieSet = driver.manage().getCookies();
            for (Cookie cookie : cookieSet) {
                System.out.println(cookie.getDomain());
                System.out.println(cookie.getName() + "=" + cookie.getValue());
                if (cookie.getExpiry() != null) {
                    System.out.println(DateFormatUtils.format(cookie.getExpiry(), "yyyy-MM-dd HH:mm:ss"));
                }
                System.out.println(cookie.getPath());
            }


            String liveUrl = "https://taobaolive.taobao.com/room/index.htm?spm=a1z9u.8142865.0.0.23e634ed2KLLw7&feedId=cfe18731-69bd-4293-834e-3ce34cafa42d";
            //不需要点击直接跳转到登陆页面
            driver.navigate().to(liveUrl);
            System.out.println("打开直播页面");

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Thread.sleep(300000);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        driver.quit();
    }


    @Test
    public void testPCWeb() {
        //此处填写驱动程序，前面是驱动名随浏览器改变，后面是你对应驱动程序的文件位置，我是绝对路径写法
        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/workspace/learning/software/chromedriver-mac-arm64/chromedriver");

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--disable-extensions");
        chromeOptions.addArguments("--profile-directory=Default");
        chromeOptions.addArguments("--incognito");
        chromeOptions.addArguments("--disable-plugins-discovery");
        chromeOptions.addArguments("--start-maximized");
        //配置以下内容可以使出现的滑块通过验证
        chromeOptions.addArguments("lang=zh-CN,zh,zh-TW,en-US,en");
        chromeOptions.addArguments("--headless");
        chromeOptions.addArguments("user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--disable-gpu");

        ChromeDriver driver = new ChromeDriver(chromeOptions);

//        ChromeDriver driver = new ChromeDriver();
        //设置隐性等待
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //网址窗口最大化
//        driver.manage().window().maximize();
        //此处填写需要打开的网址，这是淘宝的登录网址
        driver.get("https://login.taobao.com/member/login.jhtml?spm=a21tn.8216370.754894437.1.349d5722vmf9o6&f=top&redirectURL=https%3A%2F%2Ftaobaolive.taobao.com%2Froom%2Findex.htm");

        //配置以下可以是登录过程不出现滑块
        String js1 = "Object.defineProperties(navigator,{ webdriver:{ get: () => false } }) ";
        String js2 = "window.navigator.chrome = { runtime: {},  }; ";
        String js3 = "Object.defineProperty(navigator, 'languages', { get: () => ['en-US', 'en'] }); ";
        String js4 = "Object.defineProperty(navigator, 'plugins', { get: () => [1, 2, 3, 4, 5,6], }); ";
        driver.executeScript(js1);
        driver.executeScript(js2);
        driver.executeScript(js3);
        driver.executeScript(js4);

        //获得当前页面的标题
        String tile = driver.getTitle();
        System.out.println("当前页面标题：" + tile);
        //获取当前页面的url
        String url = driver.getCurrentUrl();
        System.out.println("当前页面url" + url);


        // 你的用户名
        String username = "xxxx";
        // 你的密码
        String password = "xxxx";

        //通过选择器获取元素
        WebElement usernameElement = driver.findElement(By.id("fm-login-id"));
        // 模拟用户点击用户名输入框
        usernameElement.click();
        // 设置sleep,方便观察
        Random rand = new Random();
        try {
            for (int i = 0; i < username.length(); i++) {
                // 随机睡眠0-1秒
                Thread.sleep(rand.nextInt(1000));
                // 逐个输入单个字符
                usernameElement.sendKeys("" + username.charAt(i));
            }
            WebElement passwordElement = driver.findElement(By.id("fm-login-password"));
            passwordElement.click();
            // 输入完成用户名后，随机睡眠0-3秒
            int seconds = rand.nextInt(3000);
            Thread.sleep(seconds);
            for (int i = 0; i < password.length(); i++) {
                Thread.sleep(rand.nextInt(1000));
                passwordElement.sendKeys("" + password.charAt(i));
            }

            //滑块验证
            WebElement wrapper = driver.findElement(By.id("nocaptcha-password"));
            if (wrapper.isDisplayed()) {
                Actions action = new Actions(driver);
                WebElement moveButton = driver.findElement(By.id("nc_1_n1z"));
                // 移到滑块元素并悬停,不能超出框的长度，否则异常
                action.clickAndHold(moveButton);
                action.moveByOffset(258, 0).perform();
                action.release();
            }
            //点击登录
            driver.findElement(By.className("fm-submit")).click();
            System.out.println("登录成功！");
//            Thread.sleep(3000);

            //获取cookies
            Set<Cookie> cookieSet = driver.manage().getCookies();
            for (Cookie cookie : cookieSet) {
                System.out.println(cookie.getDomain());
                System.out.println(cookie.getName() + "=" + cookie.getValue());
                if (cookie.getExpiry() != null) {
                    System.out.println(DateFormatUtils.format(cookie.getExpiry(), "yyyy-MM-dd HH:mm:ss"));
                }
                System.out.println(cookie.getPath());
            }


//            String liveUrl = "https://taobaolive.taobao.com/room/index.htm?spm=a1z9u.8142865.0.0.23e634ed2KLLw7&feedId=cfe18731-69bd-4293-834e-3ce34cafa42d";
//            //不需要点击直接跳转到登陆页面
//            driver.navigate().to(liveUrl);
            System.out.println("打开直播页面");

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Thread.sleep(300000);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        driver.quit();
    }


    @Test
    public void testLiveDetail() throws IOException {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36");
        headerMap.put("referer", "https://taobaolive.taobao.com/room/index.htm?spm=a21tn.8216370.2278280.2.642c5722PsrbV5&feedId=8943c5f1-14a8-43fc-80fc-fc18613587ed");

        Map<String, String> cookieMap = new HashMap<>();
        cookieMap.put("EGG_SESS", "W-W7q7P4VlRlXpllncZvWFGmi2qUwUidJQzh4r2_njVBMfO34rp16GUx8_kaJqr9tw_y9BDKmkUFG8tFDf7qoP2Okx2TcgXNQJ-00TUiWy5O3yKdLrMkAk2E8NO3sa");

        String liveDetail = Jsoup.connect("https://taobaolive.taobao.com/api/live_detail/1.0?creatorId=&liveId=8943c5f1-14a8-43fc-80fc-fc18613587ed")
                .ignoreContentType(true)
                .headers(headerMap)
                .cookies(cookieMap)
                .execute().body();

        System.out.println(liveDetail);

    }

}
