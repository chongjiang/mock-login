package com.mgy.mock;

import org.openqa.selenium.chrome.ChromeDriver;

@FunctionalInterface
public interface CustomConsumer {

    void accept(ChromeDriver driver, String pageSource);

}
