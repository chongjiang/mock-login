package com.mgy.mock;

import com.alibaba.fastjson.JSON;
import com.mgy.common.util.IOUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import javax.imageio.ImageIO;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageInputStream;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * xxx
 * https://blog.csdn.net/qq_18219457/article/details/95943423
 * http://npm.taobao.org/mirrors/chromedriver/
 *
 * @author maguoyong
 * @date 2021/6/2
 */
public class SvgToPngTest {


    private static final String SVG = "<svg width=\"800\" height=\"800\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><g stroke=\"null\"><title>background</title><rect x=\"-1\" y=\"-1\" height=\"802\" width=\"802\" id=\"canvas_background\" fill=\"none\" stroke=\"null\"/><g id=\"canvasGrid\" display=\"none\" stroke=\"null\"><rect id=\"svg_1\" width=\"100%\" height=\"100%\" x=\"0\" y=\"0\" stroke-width=\"0\" fill=\"url(#gridpattern)\"/></g></g><g><title>Layer 1</title><image preserveAspectRatio=\"none meet\" xlink:href=\"http://pddmydesign.superboss.cc/haibao-editor-new/13/2045/pic8-XHON6F.png\" height=\"800\" width=\"800\" id=\"pic_8\"/><image preserveAspectRatio=\"none meet\" xlink:href=\"http://pddmydesign.superboss.cc/haibao-editor-new/13/2045/pic7-yuoDsR.png\" height=\"60\" width=\"454\" y=\"622\" x=\"0\" id=\"pic_7\"/><image preserveAspectRatio=\"none meet\" xlink:href=\"http://pddmydesign.superboss.cc/haibao-editor-new/13/2045/pic6-8rjhsP.png\" height=\"65\" width=\"49\" y=\"482\" x=\"751\" id=\"pic_6\"/><image preserveAspectRatio=\"none meet\" xlink:href=\"http://pddmydesign.superboss.cc/haibao-editor-new/13/2045/pic5-bkbJyi.png\" height=\"56\" width=\"44\" y=\"540\" x=\"0\" id=\"pic_5\"/><text fill=\"#d30000\" font-family=\"Source Han Sans\" stroke=\"#d30000 \" font-size=\"30\" y=\"669.66668\" x=\"93.88889\" text-anchor=\"start\" stroke-width=\"0\" xml:space=\"preserve\" id=\"txt_4\">下单立送精美礼品一份</text><text fill=\"#ffffff\" font-family=\"Source Han Sans\" stroke=\"#ffffff \" font-size=\"36.2099\" y=\"663\" x=\"33\" text-anchor=\"start\" stroke-width=\"0\" xml:space=\"preserve\" id=\"txt_3\">送</text><text fill=\"#ffeec6\" font-family=\"Source Han Sans\" stroke=\"#ffeec6 \" font-size=\"30.00001\" y=\"670.11111\" x=\"575.88894\" text-anchor=\"start\" stroke-width=\"0\" xml:space=\"preserve\" id=\"txt_1\">特惠价：</text><text fill=\"#fef7e1\" font-family=\"AliHYAiHei-Beta\" stroke=\"#fef7e1 \" font-size=\"51.16275\" y=\"769\" x=\"34\" text-anchor=\"start\" stroke-width=\"0\" xml:space=\"preserve\" id=\"txt_0\">品质保证  爆款热卖</text><text stroke=\"#ffeec6 \" price-name=\"优惠价\" xml:space=\"preserve\" text-anchor=\"start\" font-family=\"Source Han Sans\" font-size=\"73\" id=\"svg_3\" y=\"751.55814\" x=\"567.77551\" stroke-width=\"0\" fill=\"#ffeec6\">88.88</text></g></svg>";


    ChromeDriver driver = null;

    @Before
    public void init() {
        //此处填写驱动程序，前面是驱动名随浏览器改变，后面是你对应驱动程序的文件位置
        System.setProperty(
                "webdriver.chrome.driver",
                "/workspace/learning/software/chromedriver");

        ChromeOptions chromeOptions = new ChromeOptions();
//        chromeOptions.addArguments("--headless"); // 隐藏窗口
        chromeOptions.addArguments("--window-size=4000,1600");
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--disable-setuid-sandbox");
        chromeOptions.addArguments("--disable-web-security");

        driver = new ChromeDriver(chromeOptions);
    }

    @Test
    public void test1() throws InterruptedException, IOException {

        for (int i = 0; i < 5; i++) {
            new Thread(() -> {

                long t = System.currentTimeMillis();
                // 打开指定的网站
                driver.get("file:///Users/admin/Downloads/svg.svg");
                Set<String> sets = driver.getWindowHandles();
                // 将整个页面转换为图片
                byte[] pngBytes = driver.getScreenshotAs(OutputType.BYTES);

                try (FileImageOutputStream imageOutput = new FileImageOutputStream(new File("/Users/admin/Downloads/" + UUID.randomUUID().toString() + ".png"))) {
                    imageOutput.write(pngBytes, 0, pngBytes.length);//将byte写入硬盘
                } catch (Exception e) {
                    e.printStackTrace();
                }
                System.out.println(", 耗时:" + (System.currentTimeMillis() - t));
            }).start();
        }

        Thread.sleep(10000);
    }


    @Test
    public void test2() throws InterruptedException, IOException {

        long t = System.currentTimeMillis();
        // 打开指定的网站
        driver.get("file:///Users/admin/Downloads/svg.svg");
        driver.executeScript("window.open('file:///Users/admin/Downloads/svg2.svg')");

        Set<String> sets = driver.getWindowHandles();
        System.out.println(JSON.toJSONString(sets));
        for (String handle : sets) {
            driver.switchTo().window(handle);
            // 将整个页面转换为图片
            byte[] pngBytes = driver.getScreenshotAs(OutputType.BYTES);

            try (FileImageOutputStream imageOutput = new FileImageOutputStream(new File("/Users/admin/Downloads/" + UUID.randomUUID().toString() + ".png"))) {
                imageOutput.write(pngBytes, 0, pngBytes.length);//将byte写入硬盘
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println(", 耗时:" + (System.currentTimeMillis() - t));
        }


    }


    @Test
    public void test3() throws InterruptedException, IOException {

        Map<String, String> maps = new HashMap<>();

        // 打开指定的网站
        driver.get("file:///Users/admin/Downloads/svg.svg");
        Object object = driver.executeScript("window.open('file:///Users/admin/Downloads/svg2.svg')");
        Set<String> sets = driver.getWindowHandles();
        for (String id : sets) {


            byte[] pngBytes = svgToPng(driver, id, 800, 800);

            try (FileImageOutputStream imageOutput = new FileImageOutputStream(new File("/Users/admin/Downloads/" + UUID.randomUUID().toString() + ".png"))) {
                imageOutput.write(pngBytes, 0, pngBytes.length);//将byte写入硬盘
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }


    public byte[] svgToPng(ChromeDriver driver, String uuId, int width, int height) throws IOException {

        File pngFile = null;
        try {

            WebDriver webDriver = driver.switchTo().window(driver.getWindowHandle());
            webDriver.manage().window().setSize(new Dimension(width, height));
            // 将整个页面转换为图片
            pngFile = driver.getScreenshotAs(OutputType.FILE);

            return IOUtils.toByteArray(new FileInputStream(pngFile));
        } finally {
            if (pngFile != null) {
                FileUtils.deleteQuietly(pngFile);
            }
        }
    }


    @Test
    public void test4() {
        try {
            long t = System.currentTimeMillis();
            WebDriver webDriver = driver.switchTo().defaultContent();
            driver.executeScript("document.body.innerHTML='" + SVG + "'");
//            WebDriver webDriver = driver.switchTo().window(driver.getWindowHandle());
//            webDriver.manage().window().setSize(new Dimension(800, 800));

            // 将整个页面转换为图片
//            byte[] bytes = driver.getScreenshotAs(OutputType.BYTES);

            WebElement webElement = driver.findElementsByTagName("svg").get(0);
            byte[] bytes = webElement.getScreenshotAs(OutputType.BYTES);
            webDriver.close();
            System.out.println("耗时:" + (System.currentTimeMillis() - t));
            try (FileImageOutputStream imageOutput = new FileImageOutputStream(new File("/Users/admin/Downloads/" + UUID.randomUUID().toString() + ".png"))) {
                imageOutput.write(bytes, 0, bytes.length);//将byte写入硬盘
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            driver.quit();
        }
    }


    @Test
    public void test44() throws InterruptedException {
        driver.switchTo().defaultContent();
        for (int i = 0; i < 10; i++) {

//            new Thread(() -> {


            long t = System.currentTimeMillis();
            String uuid = UUID.randomUUID().toString().replace("-", "");
            String svg1 = "<svg id=\"" + uuid + "\"  " + SVG.substring(4);
            System.out.println("uuid=" + uuid);
            String js = "var div = document.createElement(\"div\");  window.document.body.appendChild(div);";
            WebElement webElement = driver.findElementsByTagName("body").get(0);
            if (webElement.isEnabled()) {
                driver.executeScript(js);
            }
            driver.switchTo().defaultContent();

//                WebElement webElement = driver.findElementById(uuid);
//                driver.switchTo().defaultContent();
//                byte[] bytes = webElement.getScreenshotAs(OutputType.BYTES);
//                System.out.println("耗时:" + (System.currentTimeMillis() - t));
//                driver.executeScript("document.getElementById(\"" + uuid + "\").remove()");
//                try (FileImageOutputStream imageOutput = new FileImageOutputStream(new File("/Users/admin/Downloads/" + uuid + ".png"))) {
//                    imageOutput.write(bytes, 0, bytes.length);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

//            }).start();


        }

        Thread.sleep(10000);
    }


    @Test
    public void test5() throws InterruptedException {
        String svg1 = "<svg width=\"750\" height=\"1000\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"> <!-- 光云科技制作 --> <g stroke=\"null\">  <title>background</title>  <rect stroke=\"null\" fill=\"none\" id=\"canvas_background\" width=\"752\" height=\"1002\" y=\"-1\" x=\"-1\"></rect>  <g stroke=\"null\" display=\"none\" id=\"canvasGrid\">   <rect fill=\"url(#gridpattern)\" stroke-width=\"0\" y=\"0\" x=\"0\" height=\"100%\" width=\"100%\" id=\"svg_1\"></rect>  </g> </g> <g>  <title>Layer 1</title>  <image id=\"pic_9\" x=\"0\" y=\"0\" width=\"750\" height=\"1000\" xlink:href=\"https://mydesign.superboss.cc/haibao-editor-new/16/3932/pic9-72fxKr.png\" preserveAspectRatio=\"none meet\"></image>  <image id=\"pic_8\" x=\"569\" y=\"0\" width=\"180\" height=\"88\" xlink:href=\"https://mydesign.superboss.cc/haibao-editor-new/16/3932/pic8-5lIzQi.png\" preserveAspectRatio=\"none meet\"></image>  <image id=\"pic_7\" x=\"607\" y=\"18\" width=\"104\" height=\"53\" xlink:href=\"https://mydesign.superboss.cc/haibao-editor-new/16/3932/pic7-IVtrzP.png\" preserveAspectRatio=\"none meet\"></image>  <image id=\"pic_6\" x=\"0\" y=\"817\" width=\"750\" height=\"182.91125\" xlink:href=\"https://mydesign.superboss.cc/haibao-editor-new/16/3932/pic6-zbVtap.png\" preserveAspectRatio=\"none meet\"></image>  <image id=\"pic_5\" x=\"29\" y=\"859\" width=\"158\" height=\"46\" xlink:href=\"https://mydesign.superboss.cc/haibao-editor-new/16/3932/pic5-U3pUPR.png\" preserveAspectRatio=\"none meet\"></image>  <text id=\"txt_3\" xml:space=\"preserve\" stroke-width=\"0\" text-anchor=\"start\" x=\"11\" y=\"968.00017\" font-size=\"32\" stroke=\"#ffddc7 \" font-family=\"Din Bold\" fill=\"#ffddc7\">¥</text>  <text id=\"txt_2\" xml:space=\"preserve\" stroke-width=\"0\" text-anchor=\"start\" x=\"39.8905\" y=\"893.32867\" font-size=\"24\" stroke=\"#c52f2a \" font-family=\"Source Han Sans\" fill=\"#c52f2a\">预定2套定金</text>  <text stroke=\"#c52f2a \" id=\"txt_1\" xml:space=\"preserve\" stroke-width=\"0\" text-anchor=\"start\" x=\"297.36983\" y=\"895.589\" font-size=\"34\" font-family=\"Source Han Sans\" fill=\"#c52f2a\">满300减40   领券再减20元</text>  <text stroke=\"#ffddc7 \" id=\"txt_0\" xml:space=\"preserve\" stroke-width=\"0\" text-anchor=\"start\" x=\"271.52067\" y=\"977.00017\" font-size=\"42\" font-family=\"Source Han Sans\" fill=\"#ffddc7\">仅限前500名  第3件0元</text>  <text price-name=\"活动价\" xml:space=\"preserve\" text-anchor=\"start\" font-family=\"Din Bold\" font-size=\"70\" id=\"svg_2\" y=\"973.00017\" x=\"31.63017\" stroke-width=\"0\" stroke=\"#ffddc7 \" fill=\"#ffddc7\">88.88</text> </g></svg>\n";


        for (int i = 0; i < 20; i++) {

            new Thread(() -> {
                String svg = SVG;
                long t = System.currentTimeMillis();
                byte[] bytes = SvgToPng.toPngByte(driver, svg, UUID.randomUUID().toString().replace("-", ""));
                System.out.println("耗时:" + (System.currentTimeMillis() - t));
                SvgToPng.toPng(bytes, "/Users/admin/Downloads/" + UUID.randomUUID().toString() + ".png");
            }).start();


        }


        Thread.sleep(10000);

        driver.quit();
    }


}
