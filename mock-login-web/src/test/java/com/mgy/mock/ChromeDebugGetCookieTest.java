package com.mgy.mock;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class ChromeDebugGetCookieTest {

    /**
     * 先启动浏览器，通过debug模式连接浏览器
     * mac电脑打开谷歌浏览器 open /Applications/Google\\ Chrome.app --args --remote-debugging-port=9222
     */
    public void startChrome(String proxyServer, Consumer<ChromeDriver> consumer) {
//        Process process = Runtime.getRuntime().exec("sudo open /Applications/Google\\ Chrome.app --args --remote-debugging-port=9222");
//        process.waitFor();
//

        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/workspace/learning/software/chromedriver-mac-arm64/chromedriver");

        ChromeOptions chromeOptions = new ChromeOptions();

        if (StringUtils.isNotBlank(proxyServer)) {
            Proxy proxy = new Proxy().setHttpProxy(proxyServer).setSslProxy(proxyServer);
            chromeOptions.setProxy(proxy);
        }
        chromeOptions.setExperimentalOption("debuggerAddress", "127.0.0.1:9222");

        ChromeDriver driver = null;
        try {
            driver = new ChromeDriver(chromeOptions);
            //设置隐性等待
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            consumer.accept(driver);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (driver != null) {
                //关闭浏览器
                driver.quit();
            }
        }
    }


    @Test
    public void getCookieTest() {

        String[] urlArr = {"https://www.taobao.com", "https://m.taobao.com", "https://www.1688.com", "https://s.1688.com",
                "https://www.aliyun.com","https://etao.com/","https://www.tmall.com/","https://ju.tmall.com/","https://www.alimama.com/index.htm",
                "https://sale.1688.com/factory/home.html","https://wjgj.1688.com/"};
        startChrome(null, (ChromeDriver driver) -> {

            while (true) {

                for (String url : urlArr) {
                    try {
                        TimeUnit.SECONDS.sleep(new Random().nextInt(10));
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                    driver.get(url);
                    Set<Cookie> cookies = driver.manage().getCookies();
                    if (cookies != null && cookies.size() > 0) {
                        List<String> cookieList = Lists.newArrayList();
                        for (Cookie cookie : cookies) {
                            if (StringUtils.equals(cookie.getName(), "cna")) {
                                cookieList.add(cookie.getName() + "=" + cookie.getValue());
                            }
                        }
                        System.out.println("页面标题：" + driver.getTitle()+"______"+ StringUtils.join(cookieList, ";"));
                    }
//                    driver.navigate().to("https://www.taobao.com");
                    driver.manage().deleteAllCookies();
                    driver.getLocalStorage().clear();
                    driver.getSessionStorage().clear();
//                driver.close();
                    if (!StringUtils.equals("阿里1688", driver.getTitle())) {
                        System.out.println("页面标题：" + driver.getTitle());
                        break;
                    }
                }
            }
        });
    }
}
