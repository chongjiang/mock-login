package com.mgy.mock;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * 免费代理ip：https://www.zdaye.com/free/1/
 * 1688验证码拦截地址：https://s.1688.com//youyuan/index.htm/_____tmd_____/punish?x5secdata=xcuK3UHRfaXwbOCia1j1KYKDF6sdl5yFqYmMd7TLXoOM8Bmbyx4kPnCMYi%2fsAqhucHw1AyPr19TLBOqAw3DLiXuDHdDdpfcLjw%2bTT7oulfYxblCkpscIrI14e%2fNeGAToJvjiOvgVRNRdkl0CqW%2f68iMeZUI28xFatCsv%2brFBfa9onJIo4Gw3FOLfEEetZmkPL1Z9Rdxzo7nNVw36slYY%2fOiEngeo2Ks5JiBtqFvvLzilJDAoWnPVCJNx2Hl4D239gcOQMSArnR85gzat3K3ybPcFyz%2bqz5JpS7DdEd1QWbpJccm9zIheB1LO1O2oJkNh3BXJXCVwwv20d1jeTWBZN2JQCLhAkqrfiv4mwOdzZjAH8%3d__bx__s.1688.com%2fyouyuan%2findex.htm&x5step=1
 */
public class ChromeDebugGetAli1688ItemTest extends AbstractFileReadWrite {

    private static String LOG_PATH = "/Users/admin/Downloads/1688/1688_item.txt";


    /**
     * 先启动浏览器，通过debug模式连接浏览器
     * mac电脑打开谷歌浏览器 open /Applications/Google\\ Chrome.app --args --remote-debugging-port=9222
     */
    public void startChrome(String proxyServer, Consumer<ChromeDriver> consumer) {
//        Process process = Runtime.getRuntime().exec("sudo open /Applications/Google\\ Chrome.app --args --remote-debugging-port=9222");
//        process.waitFor();
//

        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/workspace/learning/software/chromedriver-mac-arm64/chromedriver");

        ChromeOptions chromeOptions = new ChromeOptions();

        if (StringUtils.isNotBlank(proxyServer)) {
            Proxy proxy = new Proxy().setHttpProxy(proxyServer).setSslProxy(proxyServer);
            chromeOptions.setProxy(proxy);
        }
        chromeOptions.setExperimentalOption("debuggerAddress", "127.0.0.1:9222");

        ChromeDriver driver = null;
        try {
            driver = new ChromeDriver(chromeOptions);
            //设置隐性等待
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            consumer.accept(driver);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (driver != null) {
                //关闭浏览器
                driver.quit();
            }
        }
    }


    private void printLog(String content) {
        System.out.println(content);
        appendWriteFile(content, LOG_PATH);
    }


    /**
     * https://s.1688.com/selloffer/similar_search.html?offerIds=825273829791&imageAddress=https%3A%2F%2Fcbu01.alicdn.com%2Fimg%2Fibank%2FO1CN01dWCkdl1y4xhfIJ4Ku_!!2218392506526-0-cib.jpg&scene=similar_search&postCatPaths=126442003 126506004 1038378&sameDesignEnable=false
     */
    private String parseItemId(String url) {
        if (StringUtils.isBlank(url)) {
            return null;
        }
        url = url.substring(url.indexOf("?") + 1);
        String[] arr = url.split("&");
        for (String s : arr) {
            String[] kv = s.split("=");
            if (StringUtils.equals(kv[0], "offerIds")) {
                return kv[1];
            }
        }
        return null;
    }


    public void checkVerificationCode(ChromeDriver driver) {
        //滑块验证
        String title = driver.getTitle();
        if (title != null && title.contains("验证码")) {
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (Exception e) {
            }
            Actions action = new Actions(driver);
            WebElement moveButton = driver.findElement(By.id("nc_1_n1z"));
            // 移到滑块元素并悬停,不能超出框的长度，否则异常
            action.clickAndHold(moveButton);
            action.moveByOffset(258, 0).perform();
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (Exception e) {
            }
            action.release();
        }
    }


    public void capture1688ProductInfo(ChromeDriver driver, int count) {
        WebElement offerListElement = null;
        try {
            if (driver.getTitle() != null && driver.getTitle().contains("验证码")) {
                System.out.println("验证码拦截，请手动处理");
                throw new RuntimeException("验证码拦截");
            }
            offerListElement = driver.findElement(By.id("sm-offer-list"));
            if (offerListElement == null || count <= 0) {
                System.out.println("商品列表没有找到");
                return;
            }
        } catch (NoSuchElementException e) {
            e.printStackTrace();
            throw e;
        } catch (RuntimeException e) {
            throw e;
        }
        List<WebElement> list = offerListElement.findElements(By.className("space-offer-card-box"));
        list = list.stream().limit(Math.min(count, list.size())).collect(Collectors.toList());
        list.forEach(div -> {
            try {
                //商品图片
                WebElement imgContainer = div.findElement(By.className("img-container"));

                // 使用Actions类来执行hover操作
                Actions actions = new Actions(driver);
                actions.moveToElement(imgContainer).perform();
                //商品id
                WebElement cardElementHover = div.findElement(By.className("card-element-hover"));
                WebElement hoverElement = cardElementHover.findElement(By.xpath("div[@class='tag-container']/a"));
                //https://s.1688.com/selloffer/similar_search.html?offerIds=825273829791&imageAddress=https%3A%2F%2Fcbu01.alicdn.com%2Fimg%2Fibank%2FO1CN01dWCkdl1y4xhfIJ4Ku_!!2218392506526-0-cib.jpg&scene=similar_search&postCatPaths=126442003 126506004 1038378&sameDesignEnable=false
                String href = hoverElement.getAttribute("href");
                String itemId = parseItemId(href);
                printLog("\n商品id:" + itemId);

                //标题
                WebElement mojarElementTitle = div.findElement(By.className("mojar-element-title"));
                WebElement titleElement = mojarElementTitle.findElement(By.xpath("a/div"));
                String title = titleElement.getText();
                printLog("\n商品标题:" + title);

                //商品图片
//                WebElement imgContainer = div.findElement(By.className("img-container"));
                WebElement aElement = imgContainer.findElement(By.xpath("div/a/div"));
                //background-image: url(&quot;https://cbu01.alicdn.com/img/ibank/O1CN01kHbCe82002O2nYu5k_!!2929326786-0-cib.290x290.jpg?_=2020&quot;);
                String style = aElement.getAttribute("style");
                String imgUrl = style.substring(style.indexOf("https://"), style.lastIndexOf(");") - 1);
                printLog("\n商品图片:" + imgUrl);


                //价格
                WebElement mojarElementPrice = div.findElement(By.className("mojar-element-price"));
                WebElement priceElement = mojarElementPrice.findElement(By.xpath("div/div[2]"));
                String price = priceElement.getText();
                printLog("\n商品价格:" + price);

                //公司
                WebElement mojarElementCompany = div.findElement(By.className("mojar-element-company"));
                WebElement companyElement = mojarElementCompany.findElement(By.xpath("div/a/div"));
                String company = companyElement.getText();
                printLog("\n公司:" + company);
                printLog("\n");
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    public void uploadImage(ChromeDriver driver, String imageFilePath) {
        List<WebElement> webElementList = driver.findElements(By.className("react-file-reader-input"));
        if (webElementList == null || webElementList.size() == 0) {
            System.out.println("没有找到上传图片的input框，可能被拦截？");
            return;
        }

        // 找到上传输入框
        WebElement uploadInput = webElementList.get(0);
        // 上传图片
        uploadInput.sendKeys(imageFilePath);
    }


    public void captureProductInfo(ChromeDriver driver, String url, String imageDir, String imageName) {
        //此处填写需要打开的网址，这是1688页面
        driver.get(url);
        String imageFilePath = imageDir + imageName;
        uploadImage(driver, imageFilePath);
        printLog("\n\n---------------【" + imageName + "】-------------------");
        capture1688ProductInfo(driver, 12);
    }

    public void captureProductInfo(String proxyServer, String url, String imageDir, List<String> imageNameList) {
        int tryCount = 3;
        long start = System.currentTimeMillis();
        startChrome(proxyServer, (ChromeDriver driver) -> {
            printLog("---------------【抓取1688商品信息】--IP:[" + proxyServer + "]--threadId:[" + Thread.currentThread().getId() + "]-------------------");
            boolean exit = false;
            for (String imageName : imageNameList) {
                int num = 0;
                while (num < tryCount) {
                    try {
                        captureProductInfo(driver, url, imageDir, imageName);
                        break;
                    } catch (NoSuchElementException e) {
                        num++;
                        System.out.println("第" + num + "次抓取失败，继续尝试");
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException ex) {
                            throw new RuntimeException(ex);
                        }
                    } catch (RuntimeException e) {
                        if (StringUtils.isNotBlank(e.getMessage()) && e.getMessage().contains("验证码")) {
                            exit = true;
                            break;
                        }
                    }
                }
                if (exit) {
                    break;
                }
            }
        });
        printLog("\n\n总耗时:" + ((System.currentTimeMillis() - start) / 1000) + "秒");
    }

    public String getProxyIp() {
        String[] proxyServerArr = {"175.178.77.128:3128", "112.51.96.118:9091", "14.23.152.222:9090", "114.55.119.190:8888", "112.246.244.197:8088"};

        int index = new Random().nextInt(proxyServerArr.length - 1);
        return proxyServerArr[index];
    }


    @Test
    public void checkVerificationCodeTest() {
        String proxyServer = getProxyIp();

        String url = "https://s.1688.com//youyuan/index.htm/_____tmd_____/punish?x5secdata=xcuK3UHRfaXwbOCia1j1KYKDF6sdl5yFqYmMd7TLXoOM8Bmbyx4kPnCMYi%2fsAqhucHw1AyPr19TLBOqAw3DLiXuDHdDdpfcLjw%2bTT7oulfYxblCkpscIrI14e%2fNeGAToJvjiOvgVRNRdkl0CqW%2f68iMeZUI28xFatCsv%2brFBfa9onJIo4Gw3FOLfEEetZmkPL1Z9Rdxzo7nNVw36slYY%2fOiEngeo2Ks5JiBtqFvvLzilJDAoWnPVCJNx2Hl4D239gcOQMSArnR85gzat3K3ybPcFyz%2bqz5JpS7DdEd1QWbpJccm9zIheB1LO1O2oJkNh3BXJXCVwwv20d1jeTWBZN2JQCLhAkqrfiv4mwOdzZjAH8%3d__bx__s.1688.com%2fyouyuan%2findex.htm&x5step=1";

        startChrome(proxyServer, (ChromeDriver driver) -> {
            //此处填写需要打开的网址，这是1688页面
            driver.get(url);
            checkVerificationCode(driver);
        });

    }


    @Test
    public void capture1688ProductInfoTest() {
        String url = "https://s.1688.com/youyuan/index.htm?tab=imageSearch&imageId=1545308195272075680&spm=a260k.home2024";
        String proxyServer = "223.113.80.158:9091";
        //proxyServer = null;

        startChrome(proxyServer, (ChromeDriver driver) -> {
            //此处填写需要打开的网址，这是1688页面
            driver.get(url);
            capture1688ProductInfo(driver, 12);

        });
    }

    /**
     * 10张图，每张图抓取12个商品-总耗时:71秒
     * 20张图，每张图抓取12个商品-总耗时:155秒
     */
    @Test
    public void uploadImageAndCaptureTest() throws InterruptedException {
        String url = "https://s.1688.com/youyuan/index.htm?tab=imageSearch";
        //原始图片目录
        String imageDir = "/Users/admin/Downloads/1688/";

//        String[] imageNameArr = {"1.jpg", "2.jpg", "3.jpg", "4.jpg", "5.jpg", "6.jpg", "7.jpg", "8.jpg", "9.jpg", "10.jpg",
//                "11.jpg", "12.jpg", "13.jpg", "14.jpg", "15.jpg", "16.jpg", "17.jpg", "18.jpg", "19.jpg", "20.jpg"};

        String[] imageNameArr = {"1.jpg", "2.jpg", "3.jpg", "4.jpg", "5.jpg", "6.jpg", "7.jpg", "8.jpg", "9.jpg", "10.jpg"};

//        String proxyServer = getProxyIp();
        String proxyServer = null;
        captureProductInfo(proxyServer, url, imageDir, Lists.newArrayList(imageNameArr));


    }



}
