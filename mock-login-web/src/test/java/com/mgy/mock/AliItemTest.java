package com.mgy.mock;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class AliItemTest {


    public void initDriver(String proxyServer, Consumer<ChromeDriver> consumer) {
        //此处填写驱动程序，前面是驱动名随浏览器改变，后面是你对应驱动程序的文件位置，我是绝对路径写法
        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/workspace/learning/software/chromedriver-mac-arm64/chromedriver");

        ChromeOptions chromeOptions = new ChromeOptions();
        //禁用扩展的选项
        chromeOptions.addArguments("--disable-extensions");
        //指定Chrome用户配置文件的目录,使用默认
        chromeOptions.addArguments("--profile-directory=Default");
        //无痕模式
        chromeOptions.addArguments("--incognito");
        //禁用浏览器的插件发现过程
        chromeOptions.addArguments("--disable-plugins-discovery");
        //在启动浏览器时最大化浏览器窗口
        chromeOptions.addArguments("--start-maximized");

        //配置以下内容可以使出现的滑块通过验证
        chromeOptions.addArguments("lang=zh-CN,zh,zh-TW,en-US,en");
        chromeOptions.addArguments("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");


        //开启无头浏览器,开启后浏览器页面不可见
//        chromeOptions.addArguments("--headless");
        //禁用沙箱
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--disable-gpu");
        chromeOptions.addArguments("--disable-blink-features=AutomationControlled");
        //关闭自动化检测
        chromeOptions.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});


        if (StringUtils.isNotBlank(proxyServer)) {
            Proxy proxy = new Proxy().setHttpProxy(proxyServer).setSslProxy(proxyServer);
            chromeOptions.setProxy(proxy);
        }

        ChromeDriver driver = null;
        try {
            driver = new ChromeDriver(chromeOptions);

            //设置隐性等待
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
            //网址窗口最大化
//        driver.manage().window().maximize();
            //此处填写需要打开的网址，这是1688页面
//            driver.get(url);

            //配置以下可以是登录过程不出现滑块
//            String js1 = "Object.defineProperties(navigator,{ webdriver:{ get: () => false } }) ";
//            String js2 = "window.navigator.chrome = { runtime: {},  }; ";
//            String js3 = "Object.defineProperty(navigator, 'languages', { get: () => ['en-US', 'en'] }); ";
//            String js4 = "Object.defineProperty(navigator, 'plugins', { get: () => [1, 2, 3, 4, 5,6], }); ";
//            driver.executeScript(js1);
//            driver.executeScript(js2);
//            driver.executeScript(js3);
//            driver.executeScript(js4);

            consumer.accept(driver);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (driver != null) {
                //关闭浏览器
//                driver.close();
                driver.quit();
            }
        }
    }


    public void captureQQ(ChromeDriver driver) {
        WebElement songListElement = driver.findElement(By.className("songlist__list"));
        if (songListElement == null) {
            System.out.println("音乐列表没有找到");
            return;
        }
        songListElement.findElements(By.className("li")).forEach(li -> {
            //图片地址 img-container
            WebElement aElement = li.findElement(By.xpath("div/div[@class='songlist__songname']/span/a[2]"));
            System.out.println("歌曲名称:" + aElement.getText());

        });
    }


    public void startChrome(String url) {
        initDriver(null, driver -> {
            driver.get(url);
            captureQQ(driver);
        });
    }


    @Test
    public void captureQQTest() throws InterruptedException {
        String url1 = "https://y.qq.com/n/ryqq/toplist/3";
        String url2 = "https://y.qq.com/n/ryqq/toplist/4";
        String url3 = "https://y.qq.com/n/ryqq/toplist/5";
        Thread thread1 = new Thread(() -> startChrome(url1));
        Thread thread2 = new Thread(() -> startChrome(url2));
        Thread thread3 = new Thread(() -> startChrome(url3));
        thread1.start();
        thread2.start();
        thread3.start();

        thread1.join();
        thread2.join();
        thread3.join();

    }

}
