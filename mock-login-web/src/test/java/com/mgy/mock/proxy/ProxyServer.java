package com.mgy.mock.proxy;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ProxyServer implements Serializable {

    private String proxyHost;
    private int proxyPort;
    private String proxyUser;
    private String proxyPassword;

}
