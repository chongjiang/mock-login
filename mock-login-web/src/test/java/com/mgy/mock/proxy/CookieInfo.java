package com.mgy.mock.proxy;

import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Map;

@Getter
@Setter
public class CookieInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String cookie;

    private Map<String, String> cookieMap = Maps.newHashMap();

    /**
     * 最新一次使用时间
     */
    private LocalDateTime lastUseTime;


    /**
     * 最新一次被绊时间
     */
    private LocalDateTime lastBanTime;
}
