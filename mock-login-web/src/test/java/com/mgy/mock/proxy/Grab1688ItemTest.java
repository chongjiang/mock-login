package com.mgy.mock.proxy;

import com.google.common.collect.Lists;
import com.mgy.mock.AbstractFileReadWrite;
import com.mgy.mock.LogDebugReject;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Grab1688ItemTest extends AbstractFileReadWrite {
    private static String LOG_PATH = "/Users/admin/Downloads/1688/1688_item_%s_%s.txt";

    private static String currentTimeMillis = DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMddHHmmss");
    private static final ConcurrentLinkedQueue<String> queue = new ConcurrentLinkedQueue<>();

    private static final ConcurrentHashMap<String, Integer> failCountMap = new ConcurrentHashMap<>();

    private static final Object countLock = new Object();

    @Before
    public void init() {
        LogDebugReject.debugReject();

        String imageUrlFile = "/Users/admin/Downloads/1688/1688_img_url.txt";
        readFile(imageUrlFile, url -> {
            url = url.trim();
            if (StringUtils.isNotBlank(url)) {
                queue.add(url);
            }
        });

        System.out.println("图片总数:" + queue.size());
    }


    private String pollImageUrl() {
        //取出并移除队列
        return queue.poll();
    }


    public void printLog(String content, String threadId) {
        String logPath = String.format(LOG_PATH, currentTimeMillis, threadId);
        System.out.println(content);
        appendWriteFile(content, logPath);
    }

    public int countIncr(String imgUrl) {
        synchronized (countLock) {
            Integer count = failCountMap.get(imgUrl);
            if (count == null) {
                failCountMap.put(imgUrl, 1);
                return 1;
            } else {
                failCountMap.put(imgUrl, count + 1);
                return count + 1;
            }
        }
    }

    @Test
    public void singleThreadGrabTest() {
        String targetDomain = "s.1688.com";
        String path = "/youyuan/index.htm?imageAddress=%s";

        long start1 = System.currentTimeMillis();
        printLog("本线程开始执行:" + Thread.currentThread().getId() + "\t" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "\n", Thread.currentThread().getId() + "");
        ProxyGrabAliItemService proxyGrabAliItemService = new ProxyGrabAliItemService();
        while (true) {
            String url = pollImageUrl();
            if (StringUtils.isBlank(url)) {
                printLog("本线程执行完毕:" + Thread.currentThread().getId() + "\t" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "\t总耗时:" + ((System.currentTimeMillis() - start1) / 1000) + "秒", Thread.currentThread().getId() + "");
                break;
            }
            try {
                CookieInfo cookieInfo = proxyGrabAliItemService.getCookies();
                GrabResult grabResult = proxyGrabAliItemService.get1688Item(targetDomain, String.format(path, url), cookieInfo);
//                GrabResult grabResult = proxyGrabAliItemService.proxyGet1688Item(targetDomain, String.format(path, url), cookieInfo);
                String lastUseTime = cookieInfo.getLastUseTime() == null ? null : cookieInfo.getLastUseTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                String lastBanTime = cookieInfo.getLastBanTime() == null ? null : cookieInfo.getLastBanTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

                printLog(cookieInfo.getCookie() + "\t上次使用时间:" + lastUseTime + "\t上次被绊时间:" + lastBanTime + "\t" + grabResult.getMessage() + "\t当前时间:" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "\t" + url + "\n", grabResult.getThreadId() + "");

                if (!grabResult.isSuccess) {
                    if (grabResult.getMessage().contains("抓取失败")) {
                        cookieInfo.setLastBanTime(LocalDateTime.now());
                    }
                    int count = countIncr(url);
                    if (count < 5) {
                        queue.add(url);
                    }
                }
                TimeUnit.SECONDS.sleep(new Random().nextInt(5) + 5);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    @Test
    public void multiThreadGrabTest() {
        String targetDomain = "s.1688.com";
        String path = "/youyuan/index.htm?imageAddress=%s";

        long start = System.currentTimeMillis();

        int threadCount = 3;

        List<Thread> threadList = Lists.newArrayList();

        for (int i = 0; i < threadCount; i++) {
            Thread thread = new Thread(() -> {
                long start1 = System.currentTimeMillis();
                printLog("本线程开始执行:" + Thread.currentThread().getId() + "\t" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "\n", Thread.currentThread().getId() + "");
                ProxyGrabAliItemService proxyGrabAliItemService = new ProxyGrabAliItemService();
                while (true) {
                    String url = pollImageUrl();
                    if (StringUtils.isBlank(url)) {
                        printLog("本线程执行完毕:" + Thread.currentThread().getId() + "\t" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "\t总耗时:" + ((System.currentTimeMillis() - start1) / 1000) + "秒\n", Thread.currentThread().getId() + "");
                        break;
                    }
                    try {
                        CookieInfo cookieInfo = proxyGrabAliItemService.getCookies();
                        GrabResult grabResult = proxyGrabAliItemService.proxyGet1688Item(targetDomain, String.format(path, url), cookieInfo);
                        String lastUseTime = cookieInfo.getLastUseTime() == null ? null : cookieInfo.getLastUseTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                        String lastBanTime = cookieInfo.getLastBanTime() == null ? null : cookieInfo.getLastBanTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));

                        printLog(cookieInfo.getCookie() + "\t上次使用时间:" + lastUseTime + "\t上次被绊时间:" + lastBanTime + "\t" + grabResult.getMessage() + "\t当前时间:" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) + "\t" + url + "\n", grabResult.getThreadId() + "");

                        if (!grabResult.isSuccess) {
                            if (grabResult.getMessage().contains("抓取失败")) {
                                cookieInfo.setLastBanTime(LocalDateTime.now());
                            }
                            int count = countIncr(url);
                            if (count < 5) {
                                queue.add(url);
                            }
                        }
                        TimeUnit.SECONDS.sleep(new Random().nextInt(5) + 5);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
            thread.start();
            threadList.add(thread);
        }

        threadList.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
        System.out.println("------------总耗时:" + ((System.currentTimeMillis() - start) / 1000) + "秒------------");
    }
}
