package com.mgy.mock.proxy;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.Random;

public class ProxyServerFactory {

    private static List<ProxyServer> proxyServers = Lists.newArrayList();


    static {
//        ProxyServer s1 = new ProxyServer();
//        s1.setProxyHost("flow.hailiangip.com");
//        s1.setProxyPort(14224);
//        s1.setProxyUser("O24082811455782163437");
//        s1.setProxyPassword("pwd=8AV897HX&pid=14&cid=132&sip=0&uid=&dip=1");
//        proxyServers.add(s1);


        ProxyServer s2 = new ProxyServer();
        s2.setProxyHost("flow.hailiangip.com");
        s2.setProxyPort(14223);
        s2.setProxyUser("O24082710442845928989");
        s2.setProxyPassword("pwd=7AV897QQ&pid=22&cid=254&sip=0&uid=&dip=1");
        proxyServers.add(s2);
    }

    public static ProxyServer getProxyServer() {
        return proxyServers.get(new Random().nextInt(proxyServers.size()));
    }


}
