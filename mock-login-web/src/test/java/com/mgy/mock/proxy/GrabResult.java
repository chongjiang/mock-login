package com.mgy.mock.proxy;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class GrabResult implements Serializable {
    boolean isSuccess = true;

    private Long threadId;

    private String message = "";

    private Integer statusCode;

}
