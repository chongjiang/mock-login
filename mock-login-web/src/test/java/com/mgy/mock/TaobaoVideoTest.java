package com.mgy.mock;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mgy.common.util.IOUtil;
import org.jsoup.Jsoup;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * xxx
 *
 * @author maguoyong
 * @date 2020/7/16
 */
public class TaobaoVideoTest {

    private ChromeDriver driver;

    @Before
    public void init() throws InterruptedException {
        //此处填写驱动程序，前面是驱动名随浏览器改变，后面是你对应驱动程序的文件位置，我是绝对路径写法
        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/gaoyuan/mgy/learning/browserDriver/83.0.4103.39/chromedriver_mac64");
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("User-Agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36");
        chromeOptions.merge(capabilities);
        driver = new ChromeDriver(chromeOptions);
        //设置隐性等待
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //网址窗口最大化
//        driver.manage().window().maximize();
        //登录淘宝
        driver.get("https://login.taobao.com/member/login.jhtml?spm=a21bo.2017.754894437.1.5af911d9J81wxm&f=top&redirectURL=https%3A%2F%2Fwww.taobao.com%2F");
        System.out.println("打开淘宝登录页面……");

        // 用户名
        String username = "1562013338@qq.com";
        //密码
        String password = "mzdmgy156";

        Thread.sleep(3000);


        //通过选择器获取元素
        WebElement usernameElement = driver.findElement(By.id("fm-login-id"));
        // 模拟用户点击用户名输入框
        usernameElement.click();
        // 设置sleep,方便观察
        Random rand = new Random();
        try {
            for (int i = 0; i < username.length(); i++) {
                // 随机睡眠0-1秒
                Thread.sleep(rand.nextInt(1000));
                // 逐个输入单个字符
                usernameElement.sendKeys("" + username.charAt(i));
            }
            WebElement passwordElement = driver.findElement(By.id("fm-login-password"));
            passwordElement.click();
            // 输入完成用户名后，随机睡眠0-3秒
            int seconds = rand.nextInt(3000);
            Thread.sleep(seconds);
            for (int i = 0; i < password.length(); i++) {
                Thread.sleep(rand.nextInt(1000));
                passwordElement.sendKeys("" + password.charAt(i));
            }
            Actions action = new Actions(driver);
            WebElement moveButton = driver.findElement(By.id("nc_1_n1z"));
            // 移到滑块元素并悬停,不能超出框的长度，否则异常
            action.clickAndHold(moveButton);
            action.moveByOffset(258, 0).perform();
            action.release();
            //点击登录
            driver.findElement(By.className("fm-submit")).click();
            System.out.println("登录成功！");
            Thread.sleep(5000);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void test1() throws InterruptedException, IOException {
        //此处填写驱动程序，前面是驱动名随浏览器改变，后面是你对应驱动程序的文件位置
        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/gaoyuan/mgy/learning/browserDriver/83.0.4103.39/chromedriver_mac64");

        Map<String, String> mobileEmulation = new HashMap<String, String>();
        mobileEmulation.put("deviceName", "iPhone X");
        Map<String, Object> chromeOptionMap = new HashMap<>();
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptionMap);

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
        chromeOptions.addArguments("User-Agent=Dalvik/2.1.0 (Linux; U; Android 10; TAS-AN00 Build/HUAWEITAS-AN00)");

        chromeOptions.merge(capabilities);

        WebDriver driver = new ChromeDriver(chromeOptions);
        //设置隐性等待
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //此处填写需要打开的网址，这是淘宝的登录网址
        driver.get("https://h5api.m.taobao.com/h5/mtop.taobao.detail.getdetail/6.0/?data=%7B%22itemId%22%3A%22609387169516%22%2C%22utdid%22%3A%221%22%2C%22id%22%3A%22609387169516%22%2C%22exParams%22%3A%22%7B%5C%22id%5C%22%3A%5C%22609387169516%5C%22%7D%22%2C%22detail_v%22%3A%228.0.0%22%2C%22itemNumId%22%3A%22609387169516%22%7D&H5Request=true&sign=23803116a30d42ddb89af86bce6ae848&jsv=2.5.8&type=jsonp&ttid=2018%40taobao_h5_9.9.9&ecode=0&AntiFlood=true&t=1594867420925&v=6.0&AntiCreep=true&callback=mtopjsonp1&appKey=12574478&isSec=0&api=mtop.taobao.detail.getdetail");
        WebElement preElement = driver.findElement(By.tagName("pre"));
        String content = preElement.getText();
        content = content.substring(content.indexOf("(") + 1);
        content = content.substring(0, content.lastIndexOf(")"));
        System.out.println(content);

        JSONObject jsonObject = JSONObject.parseObject(content);
        JSONArray jsonArray = jsonObject.getJSONObject("data").getJSONObject("rate").getJSONArray("rateList").getJSONObject(0).getJSONArray("media");
        String videoUrl = null;
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject media = jsonArray.getJSONObject(i);
            if (media.containsKey("videoUrl")) {
                videoUrl = media.getString("videoUrl");
                break;
            }
        }


//        String videoUrl="https://gw.alicdn.com/bao/uploaded//play/u/6942364b476c4f466846677362646c764f534f6167413d3d/p/1/d/sd/e/6/t/1/272084483761.mp4";
        videoUrl = "https:" + videoUrl;

        driver.navigate().to(videoUrl);


//        BufferedInputStream in = Jsoup.connect(videoUrl).timeout(10000).ignoreContentType(true).execute().bodyStream();
//        IOUtil.write(videoUrl, in);
//

        try {
            Thread.sleep(300000);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
        driver.quit();
    }


    /**
     * https://m.tb.cn/h.VINWUoy?sm=130240   淘宝
     */
    @Test
    public void test2() throws InterruptedException, IOException {
        //此处填写驱动程序，前面是驱动名随浏览器改变，后面是你对应驱动程序的文件位置
        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/gaoyuan/mgy/learning/browserDriver/83.0.4103.39/chromedriver_mac64");

        Map<String, Object> chromeOptionMap = new HashMap<>();
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptionMap);

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("User-Agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36");
        chromeOptions.merge(capabilities);

        ChromeDriver driver = new ChromeDriver(chromeOptions);
        //设置隐性等待
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        //此处填写需要打开的网址，这是淘宝的登录网址
        driver.get("https://m.tb.cn/h.VINWUoy?sm=130240");


        Thread.sleep(5000);
        WebElement webElement = driver.findElementByXPath("//video/source");
        String src = webElement.getAttribute("src");
        System.out.println(src);
        driver.navigate().to(src);

        Thread.sleep(3000);
        //https://tbm-auth.alicdn.com/e99361edd833010b/20xcKv0ChcD2n6e8Zl0/dbWYMlHeYggqOROpsgh_255413835137_hd_hq.mp4?auth_key=1594912064-0-0-6a0e149286c3fa8e5e32af2d6f7c1a34
        String url = driver.getCurrentUrl();
        BufferedInputStream in = Jsoup.connect(url).timeout(10000).ignoreContentType(true).execute().bodyStream();
        IOUtil.write("/Users/gaoyuan/Desktop/video1111.mp4", in);
        driver.quit();
    }


    /**
     * https://m.tb.cn/h.VIDFPy4?sm=6d1eb7   天猫
     */

}
