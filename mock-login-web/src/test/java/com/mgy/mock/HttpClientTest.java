package com.mgy.mock;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mgy.mock.proxy.ProxyServer;
import com.mgy.mock.proxy.ProxyServerFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CookieStore;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.*;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;


public class HttpClientTest extends AbstractFileReadWrite {
    private static String LOG_PATH = "/Users/admin/Downloads/1688/1688_item_%s_%s.txt";

    private static String currentTimeMillis = DateFormatUtils.format(System.currentTimeMillis(), "yyyyMMddHHmmss");


    @Before
    public void init() {
        LogDebugReject.debugReject();
    }


    public void printLog(String content, String threadId) {
        String logPath = String.format(LOG_PATH, currentTimeMillis, threadId);
        System.out.println(content);
        appendWriteFile(content, logPath);
    }

    public static String getUA() {
        String[] ua = {"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/128.0.0.0 Safari/537.36",
                "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:46.0) Gecko/20100101 Firefox/46.0",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.87 Safari/537.36 OPR/37.0.2178.32",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36",
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586",
                "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko",
                "Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)",
                "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)",
                "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0)",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 BIDUBrowser/8.3 Safari/537.36",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.80 Safari/537.36 Core/1.47.277.400 QQBrowser/9.4.7658.400",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 UBrowser/5.6.12150.8 Safari/537.36",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.122 Safari/537.36 SE 2.X MetaSr 1.0",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36 TheWorld 7",
                "Mozilla/5.0 (Windows NT 6.1; W…) Gecko/20100101 Firefox/60.0"};

        int index = new Random().nextInt(ua.length);
        return ua[index];
    }

    public static Map<String, String> getCommonHeaders() {
        Map<String, String> headerMap = new HashMap<>();
        headerMap.put("user-agent", getUA());
        headerMap.put("sec-ch-ua", "\"Chromium\";v=\"128\", \"Not;A=Brand\";v=\"24\", \"Google Chrome\";v=\"128\"");
        headerMap.put("sec-ch-ua-platform", "\"macOS\"");
        headerMap.put("sec-fetch-dest", "document");
        headerMap.put("sec-fetch-site", "same-site");
        headerMap.put("sec-fetch-mode", "navigate");
        headerMap.put("sec-fetch-user", "?1");
        headerMap.put("upgrade-insecure-requests", "1");
        headerMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7");
        headerMap.put("accept-language", "zh-CN,zh;q=0.9");
        headerMap.put("cache-control", "max-age=0");
        headerMap.put("priority", "u=0, i");
        headerMap.put("referer", "https://www.1688.com/");

        // 设置IP切换头
        String proxyHeadKey = "Proxy-Tunnel";
        // 设置Proxy-Tunnel
        Random random = new Random();
        int tunnel = random.nextInt(10000);
        String proxyHeadVal = String.valueOf(tunnel);
        headerMap.put(proxyHeadKey, proxyHeadVal);

        return headerMap;
    }


    public static Map<String, String> getCookies() {
        String[] cookieArr = {
                "t=bc579b58c82d66d448ab102ad20ae913;cna=0BpUH3UkW1ACAbeGbtLUYnRv;_user_vitals_session_data_={\"user_line_track\":true,\"ul_session_id\":\"uosgmux8pce\",\"last_page_id\":\"www.1688.com%2Fsho9p7pqfd\"};_m_h5_tk_enc=6d43dada24e7e714bb50f5e75ebbb36c;tfstk=f6n9uubq4pB9o9LmfAT3mSYe6mpHKcHNjfk5mSVGlXhKFY5glPwfd9NKg54gfm3FDYkqjdrDQlka0oOkZ3mxQArqLXiiHBDBh-SdCf-JqAkNd7HqOQxlk0JM0cwjGPNQARVCfOwjGkMQIWs_hGwfOpe4OSZb5S1CdRy8CSs_GU_MJS7_Zi3f-QpbG_PCciiB0PF-CZjfcDeTWmM_996IvRUTwyffOJi-FAizHmJ5kSHm5f2EM3dLAchtfyEvDMrie2GTJVdAvyuIUmaK8QQrUAGtPzi6vpGxOYuzfcOVj5ujdmNnfKjYN4DzvJoDTGNxRqig7o566Jntem3A48io2kmGZ7ec5ppduZz_KdwO_jhdzkJLp7v3IZ74JeyLZppduZz_LJFkppQVuPwF.;isg=BKWlnTbWL2mqxUs1O-ppBc8ItGff4ll0JolrQqeKYVzrvsUwbzJpRDPaTCLIvnEs;mtop_partitioned_detect=1;_m_h5_tk=fb8b040871533979d69c5d249082bdd6_1724765621739;_tb_token_=e6a7147ad38ee;cookie2=10623ee798e08478a6f130f80b8e683e;xlly_s=1",
                "t=bc579b58c82d66d448ab102ad20ae913;cna=0BpUH3UkW1ACAbeGbtLUYnRv;_user_vitals_session_data_={\"user_line_track\":true,\"ul_session_id\":\"uosgmux8pce\",\"last_page_id\":\"www.1688.com%2F4bx5guiu3z\"};_m_h5_tk_enc=6d43dada24e7e714bb50f5e75ebbb36c;tfstk=fcEwoy4wjmVI8pamTXiqLHRrr0mtA0CW7oGjn-2mCfcMfhO08xMWiI2c6Srqtc0Y5jGsgmyYa915VgwTBmnGN_sSXr7hmmhccm2dxBCxm_15AO7pa0n0l2Lhh2un9x-iInVmtMDrtIAgif0HtYk-mjVmm9ln3xxDSxmDxvD2PIEuDBkjSTRO3-tvQxuZZR7JmnreVVluIX-cZkkZab2iTnxq6LDyE-VcgniuyWrqr7s2vcwz-DDa8tYns4zg1vFV4hcgYRqi1lfBffqTKzZbttxmgy4ozz2GOekU-RZERufy0VU36z34c6s-fzw7D4qGbQhIyxPrjy5Hb7jrvIHhokEY7IYqSvHEN9W3PKqCUThf1fYvkVBtLb6iSELxSvHEN9WekE3ipvl5L95..;isg=BNTUjro-TkpTx9oGMnWoxgYvpRRGLfgXv07a0W61YN_iWXSjlj3Ip4pdXVFBoTBv;mtop_partitioned_detect=1;_m_h5_tk=fb8b040871533979d69c5d249082bdd6_1724765621739;_tb_token_=e6a7147ad38ee;cookie2=10623ee798e08478a6f130f80b8e683e;xlly_s=1",
                "t=bc579b58c82d66d448ab102ad20ae913;cna=0BpUH3UkW1ACAbeGbtLUYnRv;_user_vitals_session_data_={\"user_line_track\":true,\"ul_session_id\":\"uosgmux8pce\",\"last_page_id\":\"www.1688.com%2Fxo8vs6the6a\"};_m_h5_tk_enc=6d43dada24e7e714bb50f5e75ebbb36c;tfstk=fRy6uxx0rreE35EIofIEO2sVzfDjlP6rfniYqopwDAH9kEE-8cdvHKFIlyUIWrZcjm3Ykrg4_6WzjlDmHazfUTrG3j4DiRMvukeob8b5zTWzXpw1r-_PmkX-3qnqHc3tMBIIDcOxHfhxJMix0FdvBrEdAm0KXFHxMD3K2cL9BUMDCDSs-lsLipfrlZ33X29bjJit1CqtRKpYdWGsy83BHKeIXkY_wH9OF2FaMRkLBwLopuVUWDM5hniQNkHtfrXkIAE8vSh_hs-ooWZLaXNHbGmQCuwjVXLdfq2a5-GgeaJnWWw3hAPF5LumMWyiZR7pCYFubAPYJipxlWnA49prPjE6GHGkhDgPAMODg6gYSwnomhFS6DmFuMsBaoctxDgPAMODifnnA4SCAIAc.;isg=BLOzdl2g0ZdIPZ1DeVBHi23yQrHd6EeqhGPdVGVQD1IJZNMG7bjX-hFwGpSKRJ-i;mtop_partitioned_detect=1;_m_h5_tk=fb8b040871533979d69c5d249082bdd6_1724765621739;_tb_token_=e6a7147ad38ee;cookie2=10623ee798e08478a6f130f80b8e683e;xlly_s=1",
                "t=bc579b58c82d66d448ab102ad20ae913;cna=0BpUH3UkW1ACAbeGbtLUYnRv;_user_vitals_session_data_={\"user_line_track\":true,\"ul_session_id\":\"uosgmux8pce\",\"last_page_id\":\"www.1688.com%2F0fgiazv335bh\"};_m_h5_tk_enc=6d43dada24e7e714bb50f5e75ebbb36c;tfstk=fFzyl3qyr22j0IZn3xgEgEJNM1u-lVB64yMItWVnNYDkPU930XG6KpVheJzEn4mLVvMSL2PLQs615NN8w23lCO__ibCT22HhR2VAoKBK-O61l_SvQV33AmKm3SomHX8oZe2nnZcZnpvuKYmDnflt-v2n-sumsfYkrb0nnUi0SypqO0fwe6kNzghE4AVPXeYUuFirI7D2-O4qaAkgZx8H8Ydi_CV3K98ISle48c218E37_o0mao5HmYmaq8GYLN8oErPurYalhUMuySq_R-fH-vr0jcrgZttqgoV7umaGQeo8m8EtYy6fW4Z_1yngraJthchUQ4qVnUyG4epKiM4YJuJHY0cxgA1VilO3l15tdTMkvHnRwjk1Z0xpv0cxgA1VgHKKqjhqCs5V.;isg=BODgUqQFku6vLi7qJjHk4qojseiy6cSz49JOPVrxrPuOVYB_AvmUQ7bh6f1VZXyL;mtop_partitioned_detect=1;_m_h5_tk=fb8b040871533979d69c5d249082bdd6_1724765621739;_tb_token_=e6a7147ad38ee;cookie2=10623ee798e08478a6f130f80b8e683e;xlly_s=1",
                "t=bc579b58c82d66d448ab102ad20ae913;cna=0BpUH3UkW1ACAbeGbtLUYnRv;_user_vitals_session_data_={\"user_line_track\":true,\"ul_session_id\":\"uosgmux8pce\",\"last_page_id\":\"www.1688.com%2F0s7n8yj07im\"};_m_h5_tk_enc=6d43dada24e7e714bb50f5e75ebbb36c;tfstk=fCO2kV94i1x5Uz9G4g1Nz7zrdd5Aw6nQ3Cs1SFYGlij0GSZMUN_QjlYiDhRNqsX9hGsfb189Y4iIOXTvk1CmADGCkdtoL1Ii11Y-ZunOsDiINqk8Y6CMCTeu4_Wl2NPcoOxGqbbFql2DjiXuqw7dsGxGs4rlkwbcS1jcZ7Y79BAT4N6m8o3g10D1PtSHuvFg_QPC3MYc45mHaZSFxEjzs5SY-wzHShma16vp4CWHAfPRGeJlZZJrS8jcLKxfyhlUiiAyoIjXi0NfmILkdCtoS5jwEpXeKEm7UaJklQWWtDPNHLxvpORsVXCXdeOOKIc4vg6d7H5DU0V2xgWLXaDMwCp4jRWcyaSIz4vQSQgEJGifmReOHqQPAqX0BRBcyaSIz4yTBtbRzMgrz;isg=BCEhEK_q4z2WaU_J33Y10WP8MOs7zpXAOl1vXoP2HSiH6kG8yx6lkE-uSB7sIi34;mtop_partitioned_detect=1;_m_h5_tk=fb8b040871533979d69c5d249082bdd6_1724765621739;_tb_token_=e6a7147ad38ee;cookie2=10623ee798e08478a6f130f80b8e683e;xlly_s=1"

        };
        int index = new Random().nextInt(cookieArr.length);
        String cookie = cookieArr[index];
        Map<String, String> cookieMap = new HashMap<>();
        String[] arr = cookie.split("; ");
        for (String s : arr) {
            String[] kv = s.split("=");
            cookieMap.put(kv[0], kv[1]);
        }
        return cookieMap;
    }


    //设置代理
    public static void proxyRequest(ProxyServer proxyServer, String targetDomain, String path, Map<String, String> headerMap, Map<String, String> cookieMap, Consumer<String> consumer) throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        String proxyHost = proxyServer.getProxyHost();
        int proxyPort = proxyServer.getProxyPort();
        String proxyUser = proxyServer.getProxyUser();
        String proxyPassword = proxyServer.getProxyPassword();

        // 设置要访问的HttpHost,即是目标站点的HttpHost
        HttpHost target = new HttpHost(targetDomain, 443, "https");
        // 设置代理HttpHost
        HttpHost proxy = new HttpHost(proxyHost, proxyPort, "http");
        // 创建CredentialsProvider对象，用于存储认证信息
        CredentialsProvider credsProvider = new BasicCredentialsProvider();

        // 以及代理的用户名和密码
        credsProvider.setCredentials(
                new AuthScope(proxy),
                new UsernamePasswordCredentials(proxyUser, proxyPassword));


        // 创建SSL/TLS环境
        SSLContextBuilder builder = new SSLContextBuilder();
        builder.loadTrustMaterial(null, (chain, authType) -> true); // 信任所有证书
        SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(builder.build());

        // 配置连接管理器
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(20); // 最大连接数
        cm.setDefaultMaxPerRoute(10); // 每个路由的默认最大连接数


        try (CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslConnectionSocketFactory).setConnectionManager(cm)
                .setDefaultCredentialsProvider(credsProvider).build()) {
            RequestConfig config = RequestConfig.custom().setProxy(proxy).build();

            HttpGet httpGet = new HttpGet(path);
            httpGet.setConfig(config);
            if (headerMap != null && headerMap.size() > 0) {
                headerMap.forEach(httpGet::setHeader);
            }
            // 创建一个Cookie存储对象
            CookieStore cookieStore = new BasicCookieStore();
            // 创建一个HttpClientContext对象，并将Cookie存储设置进去
            HttpClientContext context = HttpClientContext.create();
            context.setCookieStore(cookieStore);
            if (cookieMap != null && cookieMap.size() > 0) {
                List<String> list = new ArrayList<>();
                cookieMap.forEach((k, v) -> {
                    // 设置cookie
                    cookieStore.addCookie(new BasicClientCookie(k, v));
                    list.add(k + "=" + v);
                });
                httpGet.setHeader("cookie", StringUtils.join(list, ";"));
            }


            CloseableHttpResponse response = httpClient.execute(target, httpGet, context);
            System.out.println("响应code：" + response.getStatusLine().getStatusCode());
            HttpEntity entry = response.getEntity();
            String content = new String(EntityUtils.toByteArray(entry), StandardCharsets.UTF_8);
            consumer.accept(content);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void get1688Item(ProxyServer proxyServer, String targetDomain, String path) throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        Map<String, String> headerMap = getCommonHeaders();
        Map<String, String> cookieMap = getCookies();
        proxyRequest(proxyServer, targetDomain, path, headerMap, cookieMap, content -> {
            if (content != null && content.contains("window.data.offerresultData")) {
                printLog(Thread.currentThread().getId() + "-抓取成功！\n", Thread.currentThread().getId() + "");
            } else {
                printLog(Thread.currentThread().getId() + "-抓取失败！\n", Thread.currentThread().getId() + "");
            }
        });
    }


    private List<String> getImageUrls() {
        String imageUrls = "https://img.alicdn.com/imgextra/i2/736068213/O1CN01WvQncd2AXbjuo3Aob_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2218052775515/O1CN01WNJEcO1qbvGwEdby2_!!2218052775515.jpg,https://img.alicdn.com/imgextra/i3/3035493001/O1CN01Yj9Moe1Y2VYA54dYH_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2206734148642/O1CN01nBgZ8c2Di5dx4AXq3_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/820379552/O1CN01TxmkCu2KQryOluivJ_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/3548691108/O1CN01Ut9I261K3VnnnBF6t_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2213011413431/O1CN01UNk8ax1bDRslYJ9bS_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/3548691108/O1CN01qI3WmI1K3Vmb8ThWw_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/2097002915/O1CN01j2chra1XP7EKm3VO3_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2734472558/O1CN01OEWKpw1UlbyHkb25e_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2201509334913/O1CN01iA9IRn1mACnJrQy5T_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/1796293056/O1CN01peYsG31YRhJQawZQw_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/1710152420/O1CN01z2CFQW1TkPRq4PIth_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/352469034/O1CN01KfjHO32GbcpXoYZz4_!!352469034-0-lubanu-s.jpg,https://img.alicdn.com/imgextra/i2/2217368775133/O1CN01BWkqoy1nmxyKaIzGd_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/3548691108/O1CN017TOX561K3VoBpYJfx_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/2216739591924/O1CN01SAoj6e1Q5EyKi5FSZ_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2208257197401/O1CN01ejHpFW24XiFV87wAy_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2074396583/O1CN0151WsRE1yV49ztPae8_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2213979354986/O1CN011xD0Kb1mhdjjXddGs_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/4028115911/O1CN01WSBcre1tXI60ixSGE_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/2215789763159/O1CN01QekyqC1ZCs8iLpTcS_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2066950537/O1CN01aRNLBV1Fpze1tv6DF_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2215573056218/O1CN01GfuUMK1vntggVb9mo_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/3074332748/O1CN01MkrMLg1WAdL1b48jn_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/643786370/O1CN0119FapE1wvVttOaQfu_!!643786370.jpg,https://img.alicdn.com/imgextra/i2/2493965190/O1CN0104ighw1oD4YE5NTVl_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2929464242/O1CN01fJplXI1hCshYeNcfG_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2206734148642/O1CN01xqO9Xl2Di5dvhoCxC_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/4028115911/O1CN01fj60f11tXI5VVUzqX_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2585554624/O1CN01QpyXes1k1qNmp7WQ3_!!2585554624.jpg,https://img.alicdn.com/imgextra/i2/1075252551/O1CN01hpAoCq1UiPCMnafg9_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/3692198762/O1CN01QlAt5J2Eb3HyIhJmx_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/3077734515/O1CN01BeRCRO1jDvCDql0SV_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/2208893219458/O1CN01vlmeQi2JjowgUhnLi_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2207361702217/O1CN01GAEYQq1SFR3TJUBfR_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/595098288/O1CN01199P6k2B5xPQAZWuM_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2076305976/O1CN01JRhMYD1u13eYZDcP1_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2493965190/O1CN014TQeMB1oD4Y5YEICv_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2214078219243/O1CN01bk7vPY2I9LnUk9YGr_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/2454133511/O1CN01f8s4um1bo5Q9VLmw7_!!2454133511.jpg,https://img.alicdn.com/imgextra/i2/3995202208/O1CN01agvU241SBJLxhexS4_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2215932065495/O1CN01dVpx3R1qSlEMlJfBu_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/4028115911/O1CN01rXNkcC1tXI5Ug99jX_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/443941294/O1CN01UxKDZO1LQhFkIStAd_!!443941294.jpg,https://img.alicdn.com/imgextra/i3/2216020479279/O1CN01mV02YE2IPpxhSYY9q_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2216049520667/O1CN01d24rdD1GnX4s8tKFV_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/2769363179/O1CN01P6q5QB1ZM1vWlFy6I_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/1663277566/O1CN01CjSvYu25lHb5ImVH7_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2216089990713/O1CN01fapPWB1H8bLUnw92q_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/646773884/O1CN01y3W9371eYvHXsYOwx_!!646773884.jpg,https://img.alicdn.com/imgextra/i1/2215585529641/O1CN01S9vYIW2L5dPxZ6IBN_!!2215585529641.jpg,https://img.alicdn.com/imgextra/i3/2212530119316/O1CN01B207k22IgmhRYFLXf_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2772547191/O1CN01hEfuxW22zX5dv3eab_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2181779871/O1CN01ocPzWd2MmyV2P8nQy_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/2863563150/O1CN018aOngg1Z8kYv1nGXX_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/3548691108/O1CN01ZCgn4P1K3VnRl4xLN_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/3316545144/O1CN01Wdsyzy1ns0D3YW9Er_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2200471371/O1CN01stdzUt1LzxuTa03jA_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/3035493001/O1CN01F3DUvP1Y2VWF21RjW_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/352469034/O1CN01gF8UtH2GbcvkQnXX0_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/3819428642/O1CN01b8EniD2Di5keQtWMI_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/1981881261/O1CN01f4oa2L1LBaOcgKQGg_!!1981881261.jpg,https://img.alicdn.com/imgextra/i1/278438476/O1CN01q6vYFN2CU3jRrXXRn_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/1653715271/O1CN01C3gVo81ooAKpms7xy_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/3913651758/O1CN01UPMS3H1OrDDomoj3M_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/4028115911/O1CN0184yjA31tXHuYAB5UF_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2181779871/O1CN01eFqlLb2MmyVPsR0a4_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/2106551894/O1CN01h4xQiu1PrV7b04nlD_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/2212791522358/O1CN01SHE2sN1TI0sDzoEeX_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/1031142890/O1CN01Z8qzgk1XDfbxnIMjq_!!1031142890.jpg,https://img.alicdn.com/imgextra/i2/2208798497364/O1CN012AlB5w24Gla2gfkna_!!2-item_pic.png,https://img.alicdn.com/imgextra/i1/736068213/O1CN01PgY0IM2AXbju7f06C_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/3546503837/O1CN01P2SbT01eDOXhKPeux_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/3074332748/O1CN01tfVGGy1WAdDyI8BFH_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/3900647164/O1CN01WKZg1822nAEQgMS4k_!!3900647164.jpg,https://img.alicdn.com/imgextra/i4/1653715271/O1CN01HjRr3q1ooAb13p9Iq_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2214394555821/O1CN01q5TnOP1ss4TgCmqrd_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/2200653499877/O1CN01RmYr5D2MpirkwvyJM_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2108390621/O1CN01vrwuh11GSSskIVmmo_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/278438476/O1CN0119asF82CU3zlDFa6q_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/352469034/O1CN01w0cN4o2Gbcv8TUf2J_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2213092281453/O1CN01kY0jUY1MbWS0PkYrk_!!2213092281453.jpg,https://img.alicdn.com/imgextra/i4/188188681/O1CN01G1iw3H2DzxC56WOfP_!!188188681.jpg,https://img.alicdn.com/imgextra/i3/2066950537/O1CN01ID37KM1FpzeEaDf3b_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/4028115911/O1CN01RrLjcY1tXI5pENJUD_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2108390621/O1CN01oWt8aI1GSShTU4sJe_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2215427330337/O1CN01Q4x09X1EMOQAoc78M_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/3692198762/O1CN01az9OYM2Eb3HFTbPH5_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2899582669/O1CN01hlH2Je1VaS2m89bum_!!2899582669.jpg,https://img.alicdn.com/imgextra/i1/3692198762/O1CN01ITAlVt2Eb3BBs39lu_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2231389682/O1CN01VPUGVF2LOPfwSmbrJ_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2107937413/O1CN01z2gujI24dCsB9fCw6_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2211951413695/O1CN01DR69d01dAMRXs3v6g_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2200656749041/O1CN014Ae7PL2GepfyOOoTA_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/3692198762/O1CN01OVZPP42Eb3GxPkYWV_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2108390621/O1CN01y3RRTt1GSSvkOVDk0_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2111047672/O1CN01q2X96S26XpbBRvTkn_!!2111047672.jpg,https://img.alicdn.com/imgextra/i2/2210999321722/O1CN018f33jt1Oaiz0h4IA7_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2215679580753/O1CN01pxS89R1HQvBT6rLrg_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/2199109256/O1CN0115SwSy2IFIx5pQDjU_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/2493965190/O1CN01xrVIYE1oD4YIS778X_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2217604992037/O1CN01oEXBGu1QuzkXg2qpL_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2200471371/O1CN01bUkAZZ1LzxkWX2l0g_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/1075252551/O1CN01FifK2X1UiPGFyKF1I_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2208104641258/O1CN01IPWU0C1LADCIjsDcx_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/352469034/O1CN01OzBneB2Gbd0VQRYqC_!!352469034-2-lubanu-s.png,https://img.alicdn.com/imgextra/i3/2216778333058/O1CN01C6nTU51YSc8botl4d_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/3952333893/O1CN01JjlL771ed2j3kqnZU_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2215267182560/O1CN01LA2sfH1UmWu0bZjvg_!!2215267182560.jpg,https://img.alicdn.com/imgextra/i4/4218328184/O1CN010L86Le2AKKFi6oDty_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/352804768/O1CN01zrVWJc1l5nFgmmECt_!!352804768.jpg,https://img.alicdn.com/imgextra/i2/867832552/O1CN01G3LCtI1UirWMJCbad_!!867832552.jpg,https://img.alicdn.com/imgextra/i2/2206584264218/O1CN012tB6Tn1h1ta1lIcs7_!!2206584264218-0-C2M.jpg,https://img.alicdn.com/imgextra/i3/3506816478/O1CN01woGHjV1xiyg6KBk3l_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/2769363179/O1CN01H8DPMN1ZM24PvTvpc_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/1743887798/O1CN01kEzZNU27TXKNmfrW8_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2217772298785/O1CN01Y7LlZB2ElaOtAcqZG_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2214352359650/O1CN01g9eBcN2L9kzuzLNci_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2074396583/O1CN01GP7ffX1yV3zf34XIv_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/172619986/O1CN01X44CRF2Nde3Ltckly_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/3692198762/O1CN01ZqgWWT2Eb3Hoqut3D_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/3936887433/O1CN01tKDCnX24mMhY8J9xA_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2199109256/O1CN01IEjaRf2IFIwajFqr3_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2199109256/O1CN01qMWEy42IFIvVvk7YN_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/188188681/O1CN0184DtmA2Dzx5fbQkj0_!!188188681.jpg,https://img.alicdn.com/imgextra/i4/3952333893/O1CN016v7AU21ed2qtmeubr_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/3074332748/O1CN01SrQvcf1WAdLizKZYR_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/3035493001/O1CN01lGHNDx1Y2VXGaypgF_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2201485935458/O1CN01f8e1oD1qBoOQe4snU_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/278438476/O1CN01OsIf6s2CU3s3AnxfW_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/1730779065/O1CN01vKLG1W2GppGjMNuEL_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2217581736515/O1CN01WIoLDW1xzvKNO6Hy1_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2199109256/O1CN01u75VTA2IFIx4kYSEe_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/3952333893/O1CN01rwrKSf1ed2kXVhaFH_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2199109256/O1CN01y1KqQl2IFIxBziKNl_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/2493965190/O1CN01bBt9mn1oD4YIKXDMM_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/3506816478/O1CN013GwjzE1xiyZ4TWaBe_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2217176694622/O1CN01UTCYed1k0vZFJeg33_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2206550456728/O1CN01bWUsXK1zZTfXs2BCz_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/3074332748/O1CN01BXzwaj1WAdLTaYScf_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2493965190/O1CN01eJE3PV1oD4RJNSGqX_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/3952333893/O1CN01j9sFKY1ed2oURksvU_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/3506816478/O1CN01t01RP61xiyYUlpGdS_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/3546503837/O1CN01PUKt9e1eDOYPmlzDo_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/804206007/O1CN01ECz6cD1uFG8Yw5q1w_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/201634770/O1CN01svRsgC1l6i5x2ZqzH_!!201634770.jpg,https://img.alicdn.com/imgextra/i3/352469034/O1CN015L2cXD2GbcolnDVOE_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2217604992037/O1CN01lfbt5t1Quzki0ap4U_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/3538791988/O1CN01GJhvJT1QYY8oISv0h_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2217003895368/O1CN01sEkhRd1pWb3r8FB8Y_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/1075252551/O1CN01AjWs9I1UiPIxTcB19_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2199109256/O1CN01fsKkoK2IFIx1LfHHs_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2206901608308/O1CN01ZvgSN42BF7UTv2jZl_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2200656749041/O1CN01CUjfaV2GeplGNacsZ_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/188188681/O1CN01k4gMkC2DzxCDDUJmC_!!188188681.jpg,https://img.alicdn.com/imgextra/i1/1710152420/O1CN01QeNh9L1TkPRlr3i8C_!!2-item_pic.png,https://img.alicdn.com/imgextra/i1/1710152420/O1CN01NYsfxr1TkPRY1QlgN_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2199109256/O1CN01EhIkiA2IFIx5rWK2u_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/3952333893/O1CN01ieUlaD1ed2qyEv1hA_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2214352359650/O1CN01ReWwzr2L9kzabN85L_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2209454203467/O1CN0171Qg1V1bTw7srdbVw_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2199109256/O1CN01m10JsM2IFIx7gWwud_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2181779871/O1CN01FALC3I2MmyWbQvNvb_!!2-item_pic.png,https://img.alicdn.com/imgextra/i1/2216089990713/O1CN01eghOTw1H8bIcAEoZM_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2863563150/O1CN01CwWrIY1Z8kR1zXLan_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2200558821685/O1CN01qf6rpy1OJmIno29mP_!!2200558821685.jpg,https://img.alicdn.com/imgextra/i2/2108390621/O1CN01soAsy81GSSvBUB2nm_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/2049290807/O1CN01m5CFV91Hpeah3q4RN_!!2049290807.jpg,https://img.alicdn.com/imgextra/i4/2108390621/O1CN01h0GUni1GSSvWPIPyR_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2214132067344/O1CN01w3NQQy247bWPDr8bx_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2208299977021/O1CN01AbB31E21jfkvzfjYj_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2216778333058/O1CN01bEujal1YSc8Pa7e19_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/3845825896/O1CN01AYxbZ81tQPy6d9SUg_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2181779871/O1CN01eevTnB2MmyWYAY1Og_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/2208759571041/O1CN01AyOesO1JYpB9OsKKP_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/1710152420/O1CN01UXiSn11TkPRm7d86b_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/1031142890/O1CN01XjixHJ1XDfbvx8hTa_!!1031142890.jpg,https://img.alicdn.com/imgextra/i4/3692198762/O1CN013jU5dR2Eb38iiI21K_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/3866531120/O1CN01PwXs2x1K90Xc9dJpk_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2206928169350/O1CN01EfJL112IwM95XtPZZ_!!2206928169350.jpg,https://img.alicdn.com/imgextra/i2/4028115911/O1CN01KW0iQz1tXI6kLgKly_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/1981881261/O1CN01tZRDDX1LBaO6yxlvP_!!1981881261.jpg,https://img.alicdn.com/imgextra/i2/2493965190/O1CN01DvmOTL1oD4Yx5gdnS_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/172619986/O1CN01zaM8452Nde4ChVLyM_!!172619986.jpg,https://img.alicdn.com/imgextra/i2/2211951413695/O1CN01PYBQqt1dAMRZ30lwo_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2533005469/O1CN010D45UU1qGr3WmDL8f_!!2533005469.jpg,https://img.alicdn.com/imgextra/i4/2199109256/O1CN01zdmG8a2IFIx6C3KQs_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/1710152420/O1CN01XSJk8h1TkPJfebnI3_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2108390621/O1CN011qSjP11GSSsGwGpYS_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2119128308/O1CN01ifFP7u2BF7T7ETKCs_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/1773385554/O1CN01DwctbC1qtmVDNRdAq_!!1773385554.jpg,https://img.alicdn.com/imgextra/i2/2214716392992/O1CN01br7MX31XyNrs2aIrq_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i1/3692198762/O1CN01edQkZV2Eb3HofRnOR_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2206928169350/O1CN01nCUWnX2IwMC91r9sk_!!2206928169350.jpg,https://img.alicdn.com/imgextra/i1/234597086/O1CN01M15CMB22DRT6rCR5j_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2199109256/O1CN016YD54L2IFIwnVlyET_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i4/2208299977021/O1CN01F1R7sI21jfl2xLDtV_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i3/2199109256/O1CN01V4rGCB2IFIx8eO4FW_!!0-item_pic.jpg,https://img.alicdn.com/imgextra/i2/2213011413431/O1CN01NXSqSM1bDRrI1Y0Tb_!!0-item_pic.jpg";
        return Lists.newArrayList(imageUrls.split(","));
    }


    @Test
    public void test1() throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
//        String targetDomain = "df-pre.kdzs.com";
//        String path = "/tj/testProxy?sign=xxrewrew32432**%23%40fdf";

        String targetDomain = "s.1688.com";
        String path = "/youyuan/index.htm?imageAddress=https://img.alicdn.com/imgextra/i1/2455164953/O1CN01oqPWSF1mSWghssHDg_!!0-item_pic.jpg";

        String cookie = "cna=LbFUHzaC3VICAbeGbtIDE53Y;";

        ProxyServer proxyServer = ProxyServerFactory.getProxyServer();
        Map<String, String> headerMap = getCommonHeaders();
        Map<String, String> cookieMap = Maps.newHashMap();

        String[] arr = cookie.split("; ");
        for (String s : arr) {
            String[] kv = s.split("=");
            cookieMap.put(kv[0], kv[1]);
        }


        proxyRequest(proxyServer, targetDomain, path, headerMap, cookieMap, content -> {
            System.out.println("接口返回内容:" + content);
        });

    }


    /**
     * https://s.1688.com/youyuan/index.htm?imageAddress=https://img.alicdn.com/imgextra/i1/2455164953/O1CN01oqPWSF1mSWghssHDg_!!0-item_pic.jpg
     */
    @Test
    public void testGet1688Item() throws InterruptedException, IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        String targetDomain = "s.1688.com";
        String path = "/youyuan/index.htm?imageAddress=%s";
        String imageUrl = "https://img.alicdn.com/imgextra/i1/2455164953/O1CN01oqPWSF1mSWghssHDg_!!0-item_pic.jpg";

        ProxyServer proxyServer = new ProxyServer();
        get1688Item(proxyServer, targetDomain, String.format(path, imageUrl));
    }

    /**
     * https://s.1688.com/youyuan/index.htm?imageAddress=https://img.alicdn.com/imgextra/i1/2455164953/O1CN01oqPWSF1mSWghssHDg_!!0-item_pic.jpg
     */
    @Test
    public void testBatchGet1688Item() throws InterruptedException {
        String targetDomain = "s.1688.com";
        String path = "/youyuan/index.htm?imageAddress=%s";

        ProxyServer proxyServer = new ProxyServer();

        List<String> imgUrls = getImageUrls();
        List<List<String>> list = Lists.partition(imgUrls, 70);

        long start = System.currentTimeMillis();
        List<Thread> threads = Lists.newArrayList();
        for (List<String> imgList : list) {
            Thread thread = new Thread(() -> {
                for (String url : imgList) {
                    try {
                        get1688Item(proxyServer, targetDomain, String.format(path, url));
                        TimeUnit.SECONDS.sleep(new Random().nextInt(5) + 2);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            thread.start();
            threads.add(thread);
        }

        for (Thread thread : threads) {
            thread.join();
        }

        System.out.println("------------总耗时:" + ((System.currentTimeMillis() - start) / 1000) + "秒------------");
    }

}
