package com.mgy.mock;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class StartLocalChromeTest {




    /**
     * 先启动浏览器，通过debug模式连接浏览器
     * mac电脑打开谷歌浏览器 open /Applications/Google\\ Chrome.app --args --remote-debugging-port=9222
     */
    public void startChrome(String proxyServer, Consumer<ChromeDriver> consumer) throws IOException, InterruptedException {
//        Process process = Runtime.getRuntime().exec("open '/Applications/Google Chrome.app' --args --remote-debugging-port=9222");
//        boolean b = process.waitFor(30, TimeUnit.SECONDS);
//        System.out.println("process is running: " + b);
//        System.out.println("process is running: " + process.isAlive());


        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/workspace/learning/software/chromedriver-mac-arm64/chromedriver");

        ChromeOptions chromeOptions = new ChromeOptions();
        //配置以下内容可以使出现的滑块通过验证
//        chromeOptions.addArguments("lang=zh-CN,zh,zh-TW,en-US,en");
//        chromeOptions.addArguments("user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.35");
        chromeOptions.addArguments("--user-data-dir=/Users/admin/Library/Application Support/Google/Chrome/");
        chromeOptions.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});


        if (StringUtils.isNotBlank(proxyServer)) {
            Proxy proxy = new Proxy().setHttpProxy(proxyServer).setSslProxy(proxyServer).setProxyType(Proxy.ProxyType.MANUAL);
            chromeOptions.setProxy(proxy);
        }
//        chromeOptions.setExperimentalOption("debuggerAddress", "127.0.0.1:9222");
//        chromeOptions.addArguments("--proxy-server=https://" + proxyServer);

        ChromeDriver driver = null;
        try {
            driver = new ChromeDriver(chromeOptions);
            //设置隐性等待
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

//            chromeOptions.
            //删除所有cookie
//            driver.manage().deleteAllCookies();


            consumer.accept(driver);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (driver != null) {
                //关闭浏览器
//                driver.quit();
            }
        }
    }

    @Test
    public void ipTest() throws IOException, InterruptedException {
        String url = "http://httpbin.org/ip";
        String proxyServer = "114.55.119.190:8888";
        //proxyServer = null;

        String proxyServer2 = "58.20.248.139:9002";

        startChrome(proxyServer, (ChromeDriver driver) -> {
            driver.get(url);

            System.out.println("页面内容: " + driver.getPageSource());

        });
    }


    /**
     * chrome://version/
     */
    @Test
    public void ipTest2() throws IOException, InterruptedException {
        String url = "http://httpbin.org/ip";
//        String url = "https://www.1688.com/";
        String proxyServer = "58.246.58.150:9002";
        proxyServer = null;

        startChrome(proxyServer, (ChromeDriver driver) -> {
            driver.get(url);

            System.out.println("页面内容: " + driver.getPageSource());

        });
    }


//    @Test
//    public void test3() throws MalformedURLException {
//        String proxyServer = "114.55.119.190:8888";
//        // 将代理设置添加到DesiredCapabilities对象中
//        // 设置代理
//        Proxy proxy = new Proxy();
//        proxy.setHttpProxy(proxyServer);
//        proxy.setSslProxy(proxyServer);
//        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//        capabilities.setCapability(CapabilityType.PROXY, proxy);
//
//        URL remoteAddress = new URL("http://172.16.135.16:4444/wd/hub");
//
//        // 启动Firefox浏览器
//        WebDriver driver = new RemoteWebDriver(remoteAddress, capabilities);
//
//        driver.get("http://httpbin.org/ip");
//    }

}
