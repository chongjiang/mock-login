package com.mgy.mock;

import com.alibaba.fastjson.JSON;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ntp.TimeStamp;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.CollectionUtils;

import javax.imageio.stream.FileImageOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.*;

/**
 * xxx
 * https://blog.csdn.net/qq_18219457/article/details/95943423
 * <p>
 * 驱动下载地址：https://googlechromelabs.github.io/chrome-for-testing/
 *
 * @author maguoyong
 * @date 2021/6/2
 */
public class DouyinBuyerNickGetTest {


    ChromeDriver driver = null;

    @Before
    public void init() {
        //此处填写驱动程序，前面是驱动名随浏览器改变，后面是你对应驱动程序的文件位置
        System.setProperty(
                "webdriver.chrome.driver",
                "/Users/workspace/learning/software/chromedriver");

        ChromeOptions chromeOptions = new ChromeOptions();
//        chromeOptions.addArguments("--headless"); // 隐藏窗口
        chromeOptions.addArguments("--window-size=4000,1600");
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--disable-setuid-sandbox");
        chromeOptions.addArguments("--disable-web-security");

        driver = new ChromeDriver(chromeOptions);
    }

    @Test
    public void test1() throws InterruptedException, IOException {

        long t = System.currentTimeMillis();
        // 打开指定的网站
        driver.get("file:///Users/admin/Desktop/test1/fxgNick.html");
        //点击解密
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("return document.getElementsByTagName('img')[0].click();");

        WebElement iframe = (WebElement) js.executeScript("return document.querySelector('iframe#__op__bixi_base');");
        if (iframe == null) {
            System.out.println("iframe is null");
            return;
        }
        // 切换到iframe
        driver.switchTo().frame(iframe);

        //需要等待iframe和元素加载完成
        for (int i = 0; i < 10; i++) {
            System.out.println("等待解密结果--i=" + i);
            List<WebElement> elementInIframes = driver.findElements(By.tagName("span"));
            if (elementInIframes != null && !elementInIframes.isEmpty()) {
                WebElement elementInIframe = elementInIframes.get(0);
                if (StringUtils.isNotBlank(elementInIframe.getText())) {
                    System.out.println("解密后的数据: " + elementInIframe.getText());
                    break;
                }
            }
            Thread.sleep(1000);
        }
    }


    @Test
    public void test2() throws InterruptedException, IOException {
        long t = System.currentTimeMillis();
        for (int i = 0; i < 20; i++) {
            getNick();
        }

        System.out.println("总耗时: " + (System.currentTimeMillis() - t) + ";平均耗时:" + (System.currentTimeMillis() - t) / 20);
    }


    public void getNick() {
        long t = System.currentTimeMillis();
        // 打开指定的网站
        driver.get("file:///Users/admin/Desktop/test1/fxgNick.html");
        //点击解密
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("return document.getElementsByTagName('img')[0].click();");

        // 等待并切换至动态iframe
        WebDriverWait webDriverWait = new WebDriverWait(driver, 30);
        webDriverWait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.id("__op__bixi_base")));
        // 4. 定位目标span元素并获取文本
        List<WebElement> targetSpans = webDriverWait.until(
                ExpectedConditions.visibilityOfAllElementsLocatedBy(By.tagName("span"))
        );
        if (!CollectionUtils.isEmpty(targetSpans)) {
            WebElement targetSpan = targetSpans.get(0);
            System.out.println("解密后的数据: " + targetSpan.getText());
        }
        System.out.println("耗时: " + (System.currentTimeMillis() - t));
    }

}
